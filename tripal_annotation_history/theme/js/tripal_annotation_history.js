jQuery(document).ready(function ($) { 
	
	// if u want ignore some classes, add here
	var class_array = ['views-field-transaction-date', 'views-field-transaction-type', 'views-field-transaction-user'];
	
	$('.views-field').each(function(event) {
		var class_name = $(this).attr('class').split(' ')[1];
		if (class_array.indexOf(class_name) == -1) {
			class_array.push(class_name);
			
			$('.' + class_name).each(function(event) {
				var label = $(this).children('.views-label').text();
				var value_field = $(this).children('.field-content');
				var value = value_field.text();
				
				var last_id = event + 1;
				var last_element = $('.' + class_name).get(last_id);
				if (last_element != undefined) {
					var last_label_field = last_element.getElementsByClassName('views-label')[0];
					if (last_label_field != undefined) {
						var last_label = last_label_field.innerHTML;
					}
				}
				while (last_element != undefined && last_label != label) {
					last_id += 1;
					last_element = $('.' + class_name).get(last_id);
					if (last_element != undefined) {
						last_label_field = last_element.getElementsByClassName('views-label')[0];
						if (last_label_field != undefined) {
							last_label = last_label_field.innerHTML;
						}
					}
				}
				if (last_element != undefined) {
					var last_value = last_element.getElementsByClassName('field-content')[0].innerHTML;
					if (last_value != value) {
						value_field.addClass('transaction-type-update');
					}
				}
			});
			
		}
	});
	
	
	$('.views-field-transaction-type').each(function() {
		var value_field = $(this).children('.field-content');
		var value = value_field.text();
		if (value == 'I') {
			value_field.addClass('transaction-type-insert');
		} else if (value == 'U') {
			value_field.addClass('transaction-type-update');
		} else if (value == 'D') {
			value_field.addClass('transaction-type-delete');
		}
	});
	
	
});
	