<?php if (!empty($identifier)): ?> 

<h1><?php echo t('Current Value'); ?></h1>
<?php print views_embed_view($current_data_view_name, 'default', $identifier); ?>

<h1><?php echo t('Old Values'); ?></h1>
<?php 

foreach ($transaction_groups as $transaction_group => $v) {
	echo '<h4>Transaction group #' . $transaction_group . '</h4>';
	print views_embed_view($old_data_view_name, 'default', $identifier, $transaction_group);
}
 ?>

<?php else: ?> 
  
<?php 
	$search_form = drupal_get_form('tripal_annotation_history_view_details_form');
	print drupal_render($search_form);
?>
  
<?php endif; ?>