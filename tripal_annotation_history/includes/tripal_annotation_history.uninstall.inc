<?php

/**
 * @ingroup tripal_annotation_history
 */

require_once 'tripal_annotation_history.conf.inc';
require_once 'tripal_annotation_history.util.inc';

/*
 * Uninstalls history tables for tables in array in parameter
 * Array in parameter needs to look like this : array('name_of_table1', 'name_of_table2', 'name_of_table3')
 */
function tripal_annotation_history_uninstall_tables($tables_to_uninstall = array()) {
  ksort($tables_to_uninstall);
  
  // init variables
  $schema_name = tripal_annotation_history_chado_schema_name();
  $nb_success_op = 0;
  $nb_error_op = 0;
  $nb_tables = count($tables_to_uninstall);
  
  print("\nChadoController Annotation History - Uninstall Tables \n");
  print("$nb_tables tables to uninstall \n\n");
  
  if ($nb_tables != 0) {
    foreach ($tables_to_uninstall as $table_name) {
      $table_audit_name = $schema_name . '.' . $table_name . tripal_annotation_history_table_suffix();
      $sql = 'DROP TABLE {';
      $sql .= $table_audit_name . '} CASCADE;';
      
      try {
        db_query('START TRANSACTION;');
        db_query($sql);
        db_query('DROP TRIGGER IF EXISTS ' . $schema_name . '_' . $table_name . '_audit_iud ON {' . $schema_name . '.' . $table_name . '} CASCADE;');
        db_query('DROP FUNCTION IF EXISTS ' . $schema_name . '.audit_iud_' . $table_name . '() CASCADE;');
        
        // Remove table from Tripal and delete the Drupal view if exists
        tripal_annotation_history_remove_table_to_tripal($table_name . tripal_annotation_history_table_suffix());
        $view = views_get_view(tripal_annotation_history_view_prefix() . $table_name . tripal_annotation_history_table_suffix(), true);
        if (!is_null($view)) {
          $view->delete();  
        }
        
        db_query('COMMIT;');
        
        print("[OK] $table_audit_name \n");
        $nb_success_op += 1;
      } catch (\PDOException $e) {
        db_query('ABORT;');
        
        print("[FAIL] $table_audit_name (Error message : " . $e->getMessage() . ") \n");  
        $nb_error_op += 1;
      }
    }
  }
  
  print("\nSuccess: $nb_success_op, Fail: $nb_error_op \n\n");
}

function tripal_annotation_history_remove_table_to_tripal($table_name) {
  tripal_remove_views_integration(array('table_name' => $table_name, 'priority' => 10));
  $custom_table_id = chado_get_custom_table_id($table_name);
  chado_delete_custom_table($custom_table_id);
}
