<?php

/**
 * @ingroup tripal_annotation_history
 */

require_once 'tripal_annotation_history.conf.inc';
require_once 'tripal_annotation_history.util.inc';

/*
 * Installs history tables for tables in array in parameter
 * Array in parameter needs to look like this : array('name_of_table1', 'name_of_table2', 'name_of_table3')
 * If $repair = true, function recreates triggers, indexes, functions, views for tables in the array  (function doesn't recreate tables)
 */
function tripal_annotation_history_install_tables($tables_to_install = array(), $repair = false) {
  // Init variables
  // Name of the current chado schema
  $schema_name = tripal_annotation_history_chado_schema_name();
  $nb_success_op = 0;
  $nb_error_op = 0;
  $nb_tables = count($tables_to_install);
  
  $message_type = $repair ? 'repair' : 'install';
  
  print("\nChadoController Annotation History - " . ucfirst($message_type) . " Tables \n");
  print("$nb_tables tables to " . $message_type . " \n\n");

  if ($nb_tables != 0) {
    // Clear database
    if ($repair) {
      chado_query('ANALYZE;');
    }
    
    // Install sequences and procedures
    tripal_annotation_history_install_stuff();
    
    $select = db_query('SELECT nextval(\'' . $schema_name . '.transaction_group_seq\')');
    $transaction_group = $select->fetchField();
    
    // Selection of all columns of all tables to install
    $select = db_select('information_schema.columns', 'it');
    $select->fields('it', array('table_name', 'column_name', 'data_type'))
         ->condition('it.table_schema', $schema_name)
         ->condition('it.table_name', $tables_to_install, 'IN')
         ->orderBy('it.ordinal_position', 'DESC');
    $columns = $select->execute()->fetchAll();
    
    // Selection of all indexes of all tables to install
    $select = db_select('pg_catalog.pg_indexes', 'pg');
    $select->fields('pg')
         ->condition('pg.schemaname', $schema_name)
         ->condition('pg.tablename', $tables_to_install, 'IN');
    $indexes = $select->execute()->fetchAll();
    
    // Foreign Keys
    // Query = SELECT tc.constraint_name, tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name FROM information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name WHERE tc.constraint_type = 'FOREIGN KEY'  AND tc.table_schema = 'chado' AND kcu.constraint_schema = 'chado' AND ccu.table_schema = 'chado' AND tc.table_name = 'acquisition'
    $select = db_select('information_schema.table_constraints', 'ic');
    $select->join('information_schema.key_column_usage', 'ik', 'ic.constraint_name = ik.constraint_name');
    $select->join('information_schema.constraint_column_usage', 'icc', 'ic.constraint_name = icc.constraint_name');
    $select->fields('ic', array('constraint_name', 'table_name'))
           ->fields('ik', array('column_name'))
         ->fields('icc', array('table_name', 'column_name'))
         ->condition('ic.constraint_type', 'FOREIGN KEY')
         ->condition('ic.table_schema', $schema_name)
         ->condition('ik.constraint_schema', $schema_name)
         ->condition('icc.table_schema', $schema_name)
         ->condition('ic.table_name', $tables_to_install, 'IN');
    $foreign_keys = $select->execute()->fetchAll();
    
    // Create array easier to use
    // 'table_name1' => array(
    //      'columns' => array('column_name' => 'column_type', ...), 
    //      'indexes' => array('index_name' => 'index_def', ...),
    // ), ...
    $tables = array();
    foreach ($columns as $column) {
      $tables[$column->table_name]['schema'][$column->table_name . tripal_annotation_history_table_suffix()]['fields'][$column->column_name] = array('type' => $column->data_type, 'not null' => false);
    }
    foreach ($indexes as $index) {
      $tables[$index->tablename]['indexes'][$index->indexname] = $index->indexdef;
    }
    foreach ($foreign_keys as $foreign_key) {
      $tables[$foreign_key->table_name]['foreign_keys'][$foreign_key->column_name] = array('foreign_table_name' => $foreign_key->icc_table_name . tripal_annotation_history_table_suffix(), 'foreign_column_name' => $foreign_key->icc_column_name);
    } 
    ksort($tables);
    
    // Executes every creations in transactions
    foreach ($tables as $table_name => $table_data) {
      
      $table_data['schema'][$table_name . tripal_annotation_history_table_suffix()]['fields'] = array_reverse($table_data['schema'][$table_name . tripal_annotation_history_table_suffix()]['fields']);
      $table_audit_name = $schema_name . '.' . $table_name . tripal_annotation_history_table_suffix();
      
      // CREATE TABLE QUERY
      $sql = 'CREATE TABLE {';
      $sql .= $table_audit_name . '} (';
      
      $trigger_declare = ''; // Variable use by trigger
      $trigger_insert = ''; // Variable use by trigger
      $trigger_old = ''; // Variable use by trigger
      $trigger_new = ''; // Variable use by trigger
      foreach ($table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields'] as $column_name => $column_data) {
        $sql .= $column_name . ' ' . strtoupper($column_data['type']) . ', ';
        
        $trigger_declare .= $column_name . '_var ' . strtoupper($column_data['type']) . '; ';
        $trigger_insert .= $column_name . '_var, ';
        $trigger_old  .= $column_name . '_var = OLD.' . $column_name . '; ';
        $trigger_new  .= $column_name . '_var = NEW.' . $column_name . '; ';
      }
      
      // AUDIT FIELDS
      $table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields']['transaction_type'] = array('type' => 'char', 'not null' => true);
      $sql .= 'transaction_type CHAR(1) NOT NULL, ';
      
      $table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields']['transaction_user'] = array('type' => 'name', 'not null' => true, 'default' => 'session_user');
      $sql .= 'transaction_user NAME NOT NULL DEFAULT session_user, '; //see http://www.postgresql.org/docs/8.4/interactive/datatype-character.html#DATATYPE-CHARACTER-SPECIAL-TABLE
      
      $table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields']['transaction_id'] = array('type' => 'integer', 'not null' => true, 'default' => 'NEXTVAL(\'transaction_id_seq\')');
      $sql .= 'transaction_id  INTEGER NOT NULL DEFAULT NEXTVAL(\'transaction_id_seq\'), ';
      
      $table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields']['transaction_group'] = array('type' => 'integer', 'not null' => true, 'default' => 'get_current_transaction_group()');
      $sql .= 'transaction_group INTEGER NOT NULL DEFAULT get_current_transaction_group(), ';
      
      $table_data['schema'][$table_name  . tripal_annotation_history_table_suffix()]['fields']['transaction_date'] = array('type' => 'timestamp', 'not null' => true, 'default' => 'now()');
      $sql .= 'transaction_date TIMESTAMP NOT NULL DEFAULT now()';
      
      $sql .= ');';
      
      try {
        db_query('START TRANSACTION;');
        // CREATE TABLE
        if (!$repair) {
          db_query($sql);
        }
        // GRANT
        db_query('GRANT ALL ON {' . $table_audit_name  . '} TO chadocontroller;');

        // CREATE INDEXES
        db_query('DROP INDEX IF EXISTS ' . $table_name . '_tgr_idx;');
        db_query('CREATE INDEX ' . $table_name . '_tgr_idx ON {' . $table_audit_name . '} (transaction_group);');    
        if (isset($table_data['indexes'])) {
          $idx_drop_sql = '';
          $idx_sql = '';
          foreach ($table_data['indexes'] as $idx_name => $idx_def) {
            $idx_drop_sql .= 'DROP INDEX IF EXISTS ' . $idx_name . tripal_annotation_history_table_suffix() . '; ';
            $sql_temp = $idx_def;
            $sql_temp = str_replace($idx_name, $idx_name . tripal_annotation_history_table_suffix(), $sql_temp);
            $sql_temp = str_replace(' UNIQUE ', ' ', $sql_temp);
            $sql_temp = str_replace('ON ' . $table_name . ' ', 'ON {' . $table_audit_name . '} ', $sql_temp);
            $idx_sql .= $sql_temp . '; ';
          }
          db_query($idx_drop_sql);
          db_query($idx_sql);
        }

        // INSERT OLD RECORDS 
        if (!$repair) {
          db_query('INSERT INTO {' . $table_audit_name . '} SELECT *, \'i\', session_user, NEXTVAL(\'transaction_id_seq\'), ' . $transaction_group . ', now() FROM ' . $schema_name . '.' . $table_name . ';');
        }
        
        //CREATE TRIGGER
        // Create the audit function for table 
        $code_insert = tripal_annotation_history_insert_code();
        $code_delete = tripal_annotation_history_delete_code();
        $code_update = tripal_annotation_history_update_code();
        db_query('CREATE OR REPLACE FUNCTION ' . $schema_name . '.audit_iud_' . $table_name . '() RETURNS trigger AS
          $a$ 
           DECLARE 
            ' . $trigger_declare . '
            transaction_type_var char;
          BEGIN
            IF TG_OP = \'DELETE\' THEN
              ' . $trigger_old . '
            ELSE
              ' . $trigger_new . '
            END IF;
            IF TG_OP = \'INSERT\' THEN
              transaction_type_var = \'' . $code_insert . '\';
            ELSIF TG_OP = \'DELETE\' THEN
              transaction_type_var = \'' . $code_delete . '\';
            ELSE
              transaction_type_var = \'' . $code_update . '\';
            END IF;

            INSERT INTO ' . $table_audit_name . ' ( ' . str_replace('_var', '', $trigger_insert) . '
              transaction_type
            ) VALUES ( ' . $trigger_insert . '
              transaction_type_var
            );

            IF TG_OP = \'DELETE\' THEN
              return OLD;
            ELSE
              return NEW;
            END IF;
          END
          $a$
          LANGUAGE plpgsql SECURITY DEFINER;'
        );

        //add the audit trigger on table
        db_query('DROP TRIGGER IF EXISTS ' . $schema_name . '_' . $table_name . '_audit_iud ON {' . $schema_name . '.' . $table_name . '};'); 
        db_query('CREATE TRIGGER ' . $schema_name . '_' . $table_name . '_audit_iud
          AFTER INSERT OR UPDATE OR DELETE ON {' . $schema_name . '.' . $table_name . '}
          FOR EACH ROW
          EXECUTE PROCEDURE audit_iud_' . $table_name . '();'
        );

        db_query('COMMIT;');
        
        tripal_annotation_history_add_table_to_tripal($table_name . tripal_annotation_history_table_suffix(), $table_data);
        
        print("[OK] $table_audit_name \n");
        $nb_success_op += 1;
      } catch (\PDOException $e) {
        // If we have a problem during the creation, the transaction is aborted
        db_query('ABORT;');
        
        print("[FAIL] $table_audit_name (Error message : " . $e->getMessage() . ") \n");
        $nb_error_op += 1;
      }
    }
  }
  
  print("\nSuccess: $nb_success_op, Fail: $nb_error_op \n\n");
}

/*
 * Installs sequences, procedures, ... which are needed to create tables 
 */
function tripal_annotation_history_install_stuff() {
  tripal_annotation_history_install_sequences();
  tripal_annotation_history_install_triggers();
}

/*
 * Installs sequences which are needed to create tables.
 */
function tripal_annotation_history_install_sequences() {
  // Name of the current chado schema
  $schema_name = tripal_annotation_history_chado_schema_name();
  // 
  $audit_tables = tripal_annotation_history_get_audit_tables();
  
  // sequence for transactions
  $sequence_name = 'transaction_id_seq';
  $select = db_select('information_schema.sequences', 'it');
  $select->fields('it', array('sequence_name'))
       ->condition('it.sequence_name', $sequence_name)
       ->condition('it.sequence_schema', $schema_name);
  $results = $select->execute()->fetchAll();  
  if(count($results) === 0) {
    db_query('CREATE SEQUENCE ' . $schema_name . '.' . $sequence_name . ' CYCLE;');
  }
  $max = 1; // max value uses by the sequence
  foreach ($audit_tables as $audit_table) {
    $select = db_query('SELECT MAX(transaction_id) FROM {' . $schema_name . '.' . $audit_table->table_name . '};');
    $result = $select->fetchField();
    if ($result > $max) {
      $max = $result;
    }
  }
  db_query('SELECT setval(\'' . $schema_name . '.' . $sequence_name . '\',' . $max . ')');
  
  // sequence for groups of transaction (start with 2 because 1 is used as initial insertion transaction group ID)
  $sequence_name = 'transaction_group_seq';
  $select = db_select('information_schema.sequences', 'it');
  $select->fields('it', array('sequence_name'))
       ->condition('it.sequence_name', $sequence_name)
       ->condition('it.sequence_schema', $schema_name);
  $results = $select->execute()->fetchAll();  
  if(count($results) === 0) {
    db_query('CREATE SEQUENCE ' . $schema_name . '.' . $sequence_name . ' START WITH 2 CYCLE;');
  }
  $max = 1;  // max value uses by the sequence
  foreach ($audit_tables as $audit_table) {
    $select = db_query('SELECT MAX(transaction_group) FROM {' . $schema_name . '.' . $audit_table->table_name . '};');
    $result = $select->fetchField();
    if ($result > $max) {
      $max = $result;
    }
  }
  db_query('SELECT setval(\'' . $schema_name . '.' . $sequence_name . '\',' . $max . ')');
}

/*
 * Installs triggers which are needed to create tables.
 */
function tripal_annotation_history_install_triggers() {
  // Name of the current chado schema
  $schema_name = tripal_annotation_history_chado_schema_name();
  
  // function used to generate a new group of transaction ID
  // This function should be called before each group of transaction generated
  // by a 'save' or a 'commit' under an annotation editor
  // (ie. "SELECT start_new_transaction_group();" should be the first query of
  // each 'save' or 'commit').
  db_query('CREATE OR REPLACE FUNCTION ' . $schema_name . '.start_new_transaction_group() RETURNS INTEGER AS
    $$
    BEGIN
      -- check if the temporary table with current transaction ID exists
      BEGIN
        EXECUTE \'DELETE FROM transaction_group_temp;\';
      EXCEPTION
        WHEN UNDEFINED_TABLE THEN
          EXECUTE \'CREATE TEMPORARY TABLE transaction_group_temp
            (
              transaction_group INTEGER NOT NULL DEFAULT NEXTVAL(\'\'transaction_group_seq\'\')
            ) ON COMMIT PRESERVE ROWS;\';
      END;
      EXECUTE \'INSERT INTO transaction_group_temp VALUES (NEXTVAL(\'\'transaction_group_seq\'\'));\';
      RETURN (SELECT -1*currval(\'transaction_group_seq\'));
    END;
    $$
    LANGUAGE plpgsql SECURITY DEFINER;'
  );

  // function used to get a group of transaction ID
  db_query('CREATE OR REPLACE FUNCTION ' . $schema_name . '.get_current_transaction_group() RETURNS INTEGER AS
    $$
    DECLARE
      trans_group INTEGER;
    BEGIN
      -- check if the temporary table with current transaction ID exists
      BEGIN
        SELECT INTO trans_group -1*transaction_group FROM transaction_group_temp LIMIT 1;
      EXCEPTION
        WHEN UNDEFINED_TABLE THEN
          SELECT INTO trans_group pg_backend_pid();
      END;

      RETURN trans_group;
    END;
    $$
    LANGUAGE plpgsql SECURITY DEFINER;'
  );
}


/*
 * Reference audit table in Tripal = Create integrated view.
 * 
 * @param $table_name
 *   Audit table name.
 *
 * @param $table_data
 *   Array witch contains data about the audit table.
 *  $table_data = array(
 *    'table_name' => array(
 *       'schema' => array(...), -- @see Drupal schema API
 *       'foreign_keys' => array(
 *         'column_name1' => array(
 *          'foreign_table_name' => '...', 
 *           'foreign_column_name' => '...'
 *         ),
 *        'column_name2' => array(...)
 *       ),
 *    ),
 *   ); 
 */
function tripal_annotation_history_add_table_to_tripal($table_name, $table_data = array()) {  
  // Add table to Tripal Custom Table (+ creates integrated view)
  chado_create_custom_table($table_name, $table_data['schema'][$table_name], TRUE);
  
  // Prepares the defn_array and updates integrated view with it
  // This update is used to fill description of each field and creates the joins
  $prio = 10;
  $fields = array();
  foreach ($table_data['schema'][$table_name]['fields'] as $column_name => $column_data) {
    
    if (strpos($column_data['type'], 'int') !== false) {
      $handler_field = $handler_filter = '_numeric';
    } else {
      $handler_field = '';
      $handler_filter = '_string';
    }
    
    $joins = array();
    if (isset($table_data['foreign_keys'][$column_name])) {
      $joins[$table_data['foreign_keys'][$column_name]['foreign_table_name']] = array('table' => $table_data['foreign_keys'][$column_name]['foreign_table_name'], 'field' => $table_data['foreign_keys'][$column_name]['foreign_column_name'], 'handler' => 'views_handler_join_chado_aggregator');
    }
    
    $fields[$column_name] = array(
      'name' => $column_name,
      'title' => ucwords(str_replace('_', ' ', $column_name)),
      'type' => strtolower($column_data['type']),
      'description' => t('TODO: please describe this field!'),
      'handlers' => array(
        'field' => array('name' => 'views_handler_field' . $handler_field),
        'filter' => array('name' => 'views_handler_filter' . $handler_filter),
        'sort' => array('name' => 'views_handler_sort'),
        'argument' => array('name' => 'views_handler_argument' . $handler_filter),
      ),
      'joins' => $joins,
    );
  }
  
  $defn_array = array(
    'table' => $table_name,
    'type' => 'chado',
    'name' => ucwords(t('ChaCo History : ') . str_replace(tripal_annotation_history_table_suffix(), t(''), $table_name)),
    'description' => t('TODO: please describe this table!'),
    'priority' => $prio,
    'base_table' => TRUE,
    'fields' => $fields,
  );
  
  // Update of the integrated view
  $setup_id = tripal_get_views_integration_setup_id($table_name, $prio);
    tripal_update_views_integration($setup_id, $defn_array);
  
}

/*
 * Create a Drupal view for table in parameter. View only contains a array with each columns of the table.
 *
 * @param $table_name
 *  Audit table name.
 *
 * @param $table_data
 *   Array witch contains data about the audit table.
 *  $table_data = array (
 *    'table_name' => array (
 *      'fields' => array(
 *          'column_name1' => ...,
 *        'coluln_name2' => ...,
*        ...
 *      ),
 *    ),
 *  );  
 *
 */
// array ('table_name' => array('fields' => array('column_name1' => array())))
function tripal_annotation_history_generate_view($table_name, $table_data = array()) {  
  $table_data = array_reverse($table_data[$table_name]['fields']);  
  
  $view = new view();
  $view->name = tripal_annotation_history_view_prefix() . $table_name;
  $view->description = t('View for table : ' . ucwords(str_replace('_', ' ', $table_name)));
  $view->tag = 'tripal annotation history';
  $view->base_table = $table_name;
  $view->human_name = t('ChaCo History : ' . $table_name);
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = t('ChaCo History : ' . $table_name);
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access chado_example content';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  foreach ($table_data as $column_name => $column_data) {
    $handler->display->display_options['style_options']['columns'][$column_name] = $column_name;
  }
  foreach ($table_data as $column_name => $column_data) {
    $handler->display->display_options['fields'][$column_name]['id'] = $column_name;
    $handler->display->display_options['fields'][$column_name]['table'] = $table_name;
    $handler->display->display_options['fields'][$column_name]['field'] = $column_name;
    
    $handler->display->display_options['style_options']['info'][$column_name] =  array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    );
  }
  $handler->display->display_options['style_options']['info'] = array(
    'transaction_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    ),
    'transaction_group' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    ),
    'transaction_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    ),
    'transaction_user' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    ),
    'transaction_type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
    ),
  );
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = tripal_annotation_history_view_url() . $table_name;
  $handler->display->display_options['menu']['title'] = 'ChaCo History : ' . $table_name;
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';
  
  $view->save();
  
  return $view;
}
