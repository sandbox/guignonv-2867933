<?php

require_once 'tripal_annotation_history.install.inc';

/*
 * Return the list of all audit tables.
 *
 * @return
 *   The list of all audit tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_get_audit_tables() {
  $select = db_select('information_schema.tables', 'it');
  $select->fields('it', array('table_name'))
       ->condition('it.table_type', 'BASE TABLE')
       ->condition('it.table_schema', tripal_annotation_history_chado_schema_name())
       ->condition('it.table_name', '%' . tripal_annotation_history_table_suffix(), 'LIKE');
  $select->orderBy('it.table_name', 'ASC');
  return $select->execute()->fetchAllAssoc('table_name');
}

/*
 * Tests if string $haystack ends with the specified string $needle.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_end_with($haystack, $needle) {
  $length = strlen($needle);
    return ($length == 0) || (substr($haystack, -$length) === $needle);
}

/*
 * Install and uninstall tables for tables in arrays in parameter. This function is used by the Tripal CRON.
 *
 * @param $tables_to_install
 *   Array which contains name of tables to install. Table name should not include the history table suffix.
 *   ex: if you want install history for feature and cv tables, array need to look like this : array('feature', 'cv')
 *
 * @param $tables_to_uninstall
 *    Array which contains name of tables to uninstall. Table name should not include the history table suffix.
 *   ex: if you want uninstall history for analysis and stock tables, array need to look like this : array('analysis', 'stock')
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_update_tables($tables_to_install = array(), $tables_to_uninstall = array()) {
  tripal_annotation_history_install_tables($tables_to_install);
  tripal_annotation_history_uninstall_tables($tables_to_uninstall);
}

/*
 * Clear history tables for tables in array in parameter. Each clear is in a SQL transaction.
 *
 * @param $tables_to_clear
 *   Array which contains name of tables to clear. Table name should not include the history table suffix.
 *    ex: if you want clear history for analysis and stock tables, array need to look like this : array('analysis', 'stock')
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_clear_tables($tables_to_clear = array()) {
  ksort($tables_to_clear);
  
  //init variables
  $nb_success_op = 0;
  $nb_error_op = 0;
  $nb_tables = count($tables_to_clear);
  
  print("\nChadoController Annotation History - Clear Tables \n");
  print("$nb_tables tables to clear \n\n");
  
  if ($nb_tables != 0) {
    foreach ($tables_to_clear as $table_name) {
      $table_audit_name = $table_name . tripal_annotation_history_table_suffix();
      $sql = 'TRUNCATE TABLE {';
      $sql .= $table_audit_name . '};';
      
      try {
        db_query('START TRANSACTION;');
        chado_query($sql);
        db_query('COMMIT;');
        
        print("[OK] $table_audit_name \n");
        $nb_success_op += 1;
      } catch (\PDOException $e) {
        db_query('ABORT;');
        
        print("[FAIL] $table_audit_name (Error message : " . $e->getMessage() . ") \n");  
        $nb_error_op += 1;
      }
    }
    
    if ($nb_error_op == 0) {
      tripal_annotation_history_install_sequences();
    }
  }
  
  print("\nSuccess: $nb_success_op, Fail: $nb_error_op \n\n");
  
}