<?php

/**
 * @ingroup tripal_annotation_history
 */

require_once 'tripal_annotation_history.conf.inc';
require_once 'tripal_annotation_history.util.inc';

require_once 'tripal_annotation_history.install.inc';
require_once 'tripal_annotation_history.uninstall.inc';

/*
 * Implement hook_form().
 *
 * This form provides fields to filter user to display for the user specific page.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_user_specific_filter_form($form, $form_state) {
  // Add title and HTML class to the form
  $form = array(
    '#prefix' => '<h1>' . t('Search') . '</h1>',
    '#attributes' => array('class' => 'cc-annotation-history-exposed-form'),
  );

  // Text input to search by name
  $form['cc-name-filter'] = array(
    '#field_prefix' => t('Chado User : '),
    '#type' => 'textfield',
    '#value' => isset($_GET['username']) ? $_GET['username'] : '',
  );
  
  $form['cc-date-begin-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' Date : from '),
    '#value' => isset($_GET['dateB']) ? $_GET['dateB'] : '',
  );
  
  $form['cc-date-end-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' to '),
    '#value' => isset($_GET['dateE']) ? $_GET['dateE'] : '',
  );
  
  // Submit button for the filter form
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit-filter',
    '#value' => t('Search'),
    '#prefix' => '<div class="cc-annotation-history-clear"></div>',
  );
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * Add the filters as URL parameters.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_user_specific_filter_form_submit($form, $form_state) {
  drupal_goto('admin/tripal/extension/tripal_annotation_history/user_specific', 
    array('query' => array(
	  'username' => $form_state['input']['cc-name-filter'], 
	  'dateB' => $form_state['input']['cc-date-begin-filter'], 
	  'dateE' => $form_state['input']['cc-date-end-filter'])
	)
  ); 
}

/*
 * Implement hook_form().
 *
 * This form provides fields to filter user to display for the user specific details page.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_user_specific_details_filter_form($form, $form_state) {
  
  // Add title and HTML class to the form
  $form = array(
    '#prefix' => '<h1>' . t('Search') . '</h1>',
    '#attributes' => array('class' => 'cc-annotation-history-exposed-form'),
  );

  // Text input to search by name
  $form['cc-name-filter'] = array(
    '#markup' => '<p>' . t('Chado User : ') . (isset($_GET['username']) ? $_GET['username'] : '') . '</p>',
    
  );
  
  $form['cc-table-filter'] = array(
    '#markup' => '<p>' . t('Table : ') . (isset($_GET['table']) ? str_replace(tripal_annotation_history_table_suffix(), '', $_GET['table']) : '') . '</p>',
    
  );
  
  $form['cc-date-begin-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' Date : from '),
    '#value' => isset($_GET['dateB']) ? $_GET['dateB'] : '',
  );
  
  $form['cc-date-end-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' to '),
    '#value' => isset($_GET['dateE']) ? $_GET['dateE'] : '',
  );
  
  // Submit button for the filter form
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit-filter',
    '#value' => t('Search'),
    '#prefix' => '<div class="cc-annotation-history-clear"></div>',
  );
  
  return $form;
}


/*
 * Implement hook_form_submit().
 * 
 * Add the filters as URL parameters.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_user_specific_details_filter_form_submit($form, $form_state) {
  drupal_goto('admin/tripal/extension/tripal_annotation_history/user_specific_details', 
    array('query' => array(
	  'username' => $_GET['username'], 
	  'table' => $_GET['table'], 
	  'dateB' => $form_state['input']['cc-date-begin-filter'], 
	  'dateE' => $form_state['input']['cc-date-end-filter'])
	)
  );
}


/*
 * Implement hook_form().
 *
 * This form provides fields to filter transaction groups to display for the transaction group specific page.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_transaction_group_specific_filter_form($form, $form_state) {
  // Add title and HTML class to the form
  $form = array(
    '#prefix' => '<h1>' . t('Search') . '</h1>',
    '#attributes' => array('class' => 'cc-annotation-history-exposed-form'),
  );

  // Text input to search by name
  $form['cc-name-filter'] = array(
    '#field_prefix' => t('Chado User : '),
    '#type' => 'textfield',
    '#value' => isset($_GET['username']) ? $_GET['username'] : '',
  );
  
  $form['cc-date-begin-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' Date : from '),
    '#value' => isset($_GET['dateB']) ? $_GET['dateB'] : '',
  );
  
  $form['cc-date-end-filter'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => 'ex: 2015-02-19'),
    '#size' => 15,
    '#field_prefix' => t(' to '),
    '#value' => isset($_GET['dateE']) ? $_GET['dateE'] : '',
  );
  
  // Submit button for the filter form
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit-filter',
    '#value' => t('Search'),
    '#prefix' => '<div class="cc-annotation-history-clear"></div>',
  );
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * Add the filters as URL parameters.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_transaction_group_specific_filter_form_submit($form, $form_state) {
  drupal_goto('admin/tripal/extension/tripal_annotation_history/transaction_group_specific',  
    array('query' => array(
	  'username' => $form_state['input']['cc-name-filter'], 
	  'dateB' => $form_state['input']['cc-date-begin-filter'], 
	  'dateE' => $form_state['input']['cc-date-end-filter'])
	)
  );
}

/*
 * Implement hook_form().
 *
 * This form provides fields to filter item to display for the item specific details page.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_view_details_form() {
  // Add title and HTML class to the form
  $form = array(
    '#prefix' => '<h1>' . t('Search') . '</h1>',
    '#attributes' => array('class' => 'cc-annotation-history-exposed-form'),
  );

  // Text input to search by id
  $form['cc-id-filter'] = array(
    '#field_prefix' => t('Identifier : '),
    '#type' => 'textfield',
  );
  
  // Submit button for the filter form
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit-filter',
    '#value' => t('Search'),
    '#prefix' => '<div class="cc-annotation-history-clear"></div>',
  );
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * Add the filters as URL parameters.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_view_details_form_submit($form, $form_state) {
  drupal_goto(request_path() . '/' . $form_state['input']['cc-id-filter']);
}

 
