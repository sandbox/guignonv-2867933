<?php

/*
 * Return the suffix to apply on all history tables.
 *
 * @return
 *   The suffix to apply on all history tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_table_suffix() {
  return '_audit';
}

/*
 * Return the prefix to apply on all views of history tables.
 *
 * @return
 *   The prefix to apply on all views of history tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_view_prefix() {
  return 'tripal_annotation_history_';
}

/*
 * Return the common part of the url of all history views.
 *
 * @return
 *   The common part of url of all history views.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_view_url() {
  return 'admin/tripal/chado/tripal_annotation_history/';
}

/*
 * Return the name of the current chado schema.
 *
 * @return
 *   The name of the current chado schema.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_chado_schema_name() {
  return isset($GLOBALS['chado_active_db']) ? $GLOBALS['chado_active_db'] : 'chado';
}

/*
 * Return the insert code of insert transaction in audit tables.
 *
 * @return
 *   The insert code of insert transaction in audit tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_insert_code() {
  return 'I';
}

/*
 * Return the update code of update transaction in audit tables.
 *
 * @return
 *   The update code of update transaction in audit tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_update_code() {
  return 'U';
}

/*
 * Return the delete code of delete transaction in audit tables.
 *
 * @return
 *   The delete code of delete transaction in audit tables.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_delete_code() {
  return 'D';
}