<?php

/**
 * @ingroup tripal_annotation_history
 */

require_once 'tripal_annotation_history.conf.inc';
require_once 'tripal_annotation_history.util.inc';

require_once 'tripal_annotation_history.install.inc';
require_once 'tripal_annotation_history.uninstall.inc';
 
 /*
 * Implement hook_form().
 *
 * This form provides four buttons. 
 * One for install all audit tables. An other for uninstall all audit tables.
 * An other for clear all audit tables. And the last one for repair history.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_form($form, $form_state) {
  if (!isset($form_state['storage']['confirm'])) {

    // Install all button
    $form['install-button'] = array(
      '#type' => 'submit',
      '#id' => 'install-all-submit',
      '#value' => t('Install history on all'),
      
    );
    
    // Uninstall all button
    $form['uninstall-button'] = array(
      '#type' => 'submit',
      '#id' => 'uninstall-all-submit',
      '#value' => t('Uninstall history on all'),
      
    );
    
    // Clear button
    $form['clear-button'] = array(
      '#type' => 'submit',
      '#id' => 'clear-all-submit',
      '#value' => t('Clear history for all'),
      
    );
    
    // Repair button
    $form['repair-button'] = array(
      '#type' => 'submit',
      '#id' => 'repair-submit',
      '#value' => t('Repair history integration'),
      
    );
	
  } else {
	$path = current_path();
	if ($form_state['storage']['confirm'] == 'clear') {
	  return confirm_form($form,
	    t('Are you sure you want to clear all history tables ?'),
	    $path,
	    '<h1>' . t('Clear history for all') . '</h1>' . t('This action cannot be undone.'),
	    t('Clear history for all'),
	    t('Cancel')
	  );	
	} elseif ($form_state['storage']['confirm'] == 'uninstall') {
      return confirm_form($form,
	    t('Are you sure you want to uninstall all history tables ?'),
	    $path,
	    '<h1>' . t('Uninstall history on all') . '</h1>' . t('This action cannot be undone.'),
	    t('Uninstall history on all'),
	    t('Cancel')
	  );	
	}
  }
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#id'] === 'uninstall-all-submit') {
	$form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['confirm'] = 'uninstall'; // this will cause the form to be rebuilt, entering the confirm part of the form
    $form_state['rebuild'] = TRUE;
	return;
  }	  
  
  if ($form_state['clicked_button']['#id'] === 'clear-all-submit') {
	$form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['confirm'] = 'clear'; // this will cause the form to be rebuilt, entering the confirm part of the form
    $form_state['rebuild'] = TRUE;
	return;
  }	  
  
  // Name of current chado schema
  $schema_name = tripal_annotation_history_chado_schema_name();

  // Selection of all tables in the current chado schema
  $select = db_select('information_schema.tables', 'it');
  $select->fields('it', array('table_name'))
       ->condition('it.table_type', 'BASE TABLE')
       ->condition('it.table_schema', $schema_name);
  $select->orderBy('it.table_name', 'ASC');
  $results = $select->execute()->fetchAllAssoc('table_name');
  
  // Formatted array of tables to create
  $format_array_create = array();
  // Formatted array of tables to delete / clear / repair
  $format_array_delete = array();
  foreach($results as $result) {
    if (isset($results[$result->table_name . tripal_annotation_history_table_suffix()])) {
      $format_array_delete[] = $result->table_name;
    } else {
      if (!tripal_annotation_history_end_with($result->table_name, tripal_annotation_history_table_suffix())) {
        $format_array_create[] = $result->table_name;
      }
    }
  }
  
  // When Install all button was clicked
  if ($form_state['clicked_button']['#id'] === 'install-all-submit') {
    tripal_add_job(t("CC Annotation History : Install all tables"), 'tripal_annotation_history', 'tripal_annotation_history_install_tables', array($format_array_create), $GLOBALS['user']->uid);
  }
  
  // When Uninstall all button was clicked
  if ($form_state['clicked_button']['#value'] === 'Uninstall history on all') {
    tripal_add_job(t("CC Annotation History : Delete all tables"), 'tripal_annotation_history', 'tripal_annotation_history_uninstall_tables', array($format_array_delete), $GLOBALS['user']->uid);
  }
  
  // When Clear all button was clicked
  if ($form_state['clicked_button']['#value'] === 'Clear for all') {
    tripal_add_job(t("CC Annotation History : Clear all tables"), 'tripal_annotation_history', 'tripal_annotation_history_clear_tables', array($format_array_delete), $GLOBALS['user']->uid);
  }
  
  // When repair button was clicked
  if ($form_state['clicked_button']['#id'] === 'repair-submit') {
    tripal_add_job(t("CC Annotation History : Repair history integration"), 'tripal_annotation_history', 'tripal_annotation_history_install_tables', array($format_array_delete, true), $GLOBALS['user']->uid);
  }
  
}

/*
 * Implement hook_form().
 *
 * This form provides fields to filter tables to display.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_filter_form($form, $form_state) {
  
  // Add title and HTML class to the form
  $form = array(
    '#prefix' => '<h1>' . t('Filter') . '</h1>',
    '#attributes' => array('class' => 'cc-annotation-history-exposed-form'),
  );

  // Text input to filter tables by name
  $form['cc-name-filter'] = array(
    '#type' => 'textfield',
    '#field_prefix' => t('Chado table name contains'),
    '#value' => isset($_GET['chado-name']) ? $_GET['chado-name'] : '',
  );
  
  // Selection box + text input to filter tables by number of records 
  $form['cc-records-filter'] = array(
       '#type' => 'select',
       '#field_prefix' => t('Table has'),
       '#options' => array(
      0 => t('More than'),
      1 => t('Exactly'),
      2 => t('Less than'),
       ),
     '#value' => isset($_GET['records-order']) ? $_GET['records-order'] : '',
  );
  $form['cc-records-number-filter'] = array(
    '#type' => 'textfield',
    '#field_suffix' => t('records'),
    '#size' => 15,
    '#value' => isset($_GET['records-number']) ? $_GET['records-number'] : '',
  );
  
  // Submit button for the filter form
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit-filter',
    '#value' => t('Filter'),
    '#prefix' => '<div class="cc-annotation-history-clear"></div>',
  );
  
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * Add the filters as URL parameters.
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_filter_form_submit($form, $form_state) {
  
  drupal_goto('admin/tripal/extension/tripal_annotation_history/install/index', array('query' => array('chado-name' => $form_state['input']['cc-name-filter'], 'records-order' => $form_state['input']['cc-records-filter'], 'records-number' => $form_state['input']['cc-records-number-filter'])));
  
}


/*
 * Implement hook_form().
 *
 * This form display all tables of the current chado schema.
 * Each rows have check box.
 * The box is checked if the table has already an audit table.
 * Display tables can be filtered by the 'tripal_annotation_history_install_filter_form'.
 *
 * @return
 *   The form
 * 
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_list_form($form, $form_state) {

  // Type of table to display
  $table_type = 'BASE TABLE';
  // Name of the current chado schema
  $schema_name = tripal_annotation_history_chado_schema_name();
  
  // Selection of all tables in the current chado schema
  // query = "SELECT IT.table_name, PG.n_live_tup FROM information_schema.tables IT JOIN pg_catalog.pg_stat_all_tables PG ON IT.table_name = PG.relname WHERE PG.schemaname = IT.table_schema AND IT.table_type = :table_type AND IT.table_schema = :schema_name ORDER BY IT.table_name ASC;";
  $select = db_select('information_schema.tables', 'it')->extend('PagerDefault');
  $select->join('pg_catalog.pg_stat_all_tables', 'pg', 'it.table_name = pg.relname');
  $select->fields('it', array('table_name'))
       ->fields('pg', array('n_live_tup'))
       ->condition('it.table_type', $table_type)
       ->condition('it.table_schema', $schema_name)
       ->condition('pg.schemaname', $schema_name)
       ->condition('it.table_name', '%' . tripal_annotation_history_table_suffix(), 'NOT LIKE')
       ->limit(15);
  
  // FILTER : adds conditions to the query
  // Filter by table name
  if (isset($_GET['chado-name']) && !empty($_GET['chado-name'])) {
    $select->condition('it.table_name', '%' . $_GET['chado-name'] . '%', 'LIKE');
  }     
  // Filter by number of rows in table
  if (isset($_GET['records-number']) && !empty($_GET['records-number']) && is_numeric($_GET['records-number'])) {
    if (!isset($_GET['records-order'])) {
      $op = '<';
    } else {
      switch ($_GET['records-order']) {
        case 1:
          $op = '=';
          break;
        case 2:
          $op = '<';
          break;
        default:
          $op = '>';
          break;
      }  
    }
    
    $select->condition('pg.n_live_tup', $_GET['records-number'], $op);
  }  
       
  $select->orderBy('it.table_name', 'ASC');
  $result = $select->execute();
  
  // Selection of all history tables to know which rows need to be checked
  $audit_tables = tripal_annotation_history_get_audit_tables();
  
  // Converts the SQL result into arrays for the Drupal form module
  // Array which contains data about the records
  $table_data['data'] = array();
  // Array which contains data to know if record needs to be checked in the list
  $table_data['check'] = array();
  foreach ($result as $k => $record) {
    $table_data['data'][$record->table_name] = array(
      'table_name' => $record->table_name, // Name of table
      'nb_rows' => $record->n_live_tup, // Number of rows in table
      'history_records' => t('-'), // Number of rows in history table ('-' is display if the table doesn't have history)
      'operations' => '', // Links to the different operations (nothing is display if table doesn't have history)
    );  
    if (isset($audit_tables[$record->table_name . tripal_annotation_history_table_suffix()])) { // If table has history
      $table_data['check'][$record->table_name] = true; // Checks the box
      
      $table_data['data'][$record->table_name]['#attributes'] = array('class' => array('cc-history-audited-table')); // Adds class to the row
      
      $history_records = chado_query("SELECT COUNT(1) AS count FROM " . $record->table_name . tripal_annotation_history_table_suffix())->fetchField(); // Counts number of rows in history tables
      $table_data['data'][$record->table_name]['history_records'] = $history_records;
      
      // Display operations links
      $operations_links = array();
      // View link
      $view_name = tripal_annotation_history_view_prefix() . $record->table_name . tripal_annotation_history_table_suffix();
      $view = views_get_view($view_name);
      if (!is_null($view)) {
        // Show view
        $url = tripal_annotation_history_view_url() . $record->table_name . tripal_annotation_history_table_suffix();
        $operations_links[] = l(t('Show view'), $url);
      } else {
        // Generate view
        $url = 'admin/tripal/extension/tripal_annotation_history/install/generate_view/' . $record->table_name;
        $operations_links[] = l(t('Generate view'), $url);
      }
      // Clear link
      if ($table_data['data'][$record->table_name]['history_records'] == 0) {
        $operations_links[] = t('Clear history');
      } else {
        // Clear history
        $url = 'admin/tripal/extension/tripal_annotation_history/install/clear/' . $record->table_name;
        $operations_links[] = l(t('Clear history'), $url);
      }
      // Remove link
      // Remove history
      $url = 'admin/tripal/extension/tripal_annotation_history/install/remove/' . $record->table_name;
      $operations_links[] = l(t('Remove history'), $url);
      // Add links to the data array
      $table_data['data'][$record->table_name]['operations'] = implode(' / ', $operations_links);
    } else {
      $table_data['check'][$record->table_name] = false; //Doesn't check the box
      
      $table_data['data'][$record->table_name]['#attributes'] = array('class' => array('cc-history-not-audited-table')); // Add class to the row
    }
  }
  
  $form = array();
  // Generates the list of tables
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => array(
      'table_name' => t('Chado Table Name'), 
      'nb_rows' => t('Records'), 
      'history_records' => t('History Records'), 
      'operations' => t('Operations')
    ),
    '#options' => $table_data['data'],
    '#empty' => t('No tables found'),
    '#prefix' => '<h1>' . t('Monitored Tables') . '</h1>',
    '#default_value' => $table_data['check'],
  );

  // Adds pagination
  $form['pager'] = array('#markup' => theme('pager'));
  
  // Adds warning text zone
  $form['warning'] = array(
    '#type' => 'container',
    '#prefix' => t('<b>Warning:</b> only applies to tables of current page'),
    '#attributes' => array('style' => 'display:block; margin-bottom: 1em;')
  );
  
  // 'Install / Unistall history on selection' button 
  $form['install-button'] = array(
    '#type' => 'submit',
    '#id' => 'install-submit',
    '#value' => t('Install / Uninstall history on selection'),
    
  );
  
  // 'Clear for selection' button
  $form['clear-button'] = array(
    '#type' => 'submit',
    '#id' => 'clear-submit',
    '#value' => t('Clear history for selection'),
    
  );

  // JS to confirm the clear submit
  drupal_add_js('(function($){ $(function(){$("#clear-submit").click(function(){return confirm("This action cannot be undone.")});}); })(jQuery);', array('type' => 'inline'));
  
  return $form;
}

/*
 * Implement hook_form_submit().
 *
 * @ingroup tripal_annotation_history
 */
function tripal_annotation_history_install_list_form_submit($form, $form_state) {
  // When Clear button was clicked
  if ($form_state['clicked_button']['#id'] === 'clear-submit') {
    
    $format_array_clear = array();
    foreach ($form_state['input']['table'] as $table_name => $value) {
    
      if (!is_null($value)) {
        $format_array_clear[] = $table_name;
      }

    }
    
    // Adds of clear operation to the Tripal CRON
    $job_name = t("CC Annotation History : Clear tables (" . implode(", ", $format_array_clear) . ")");
    tripal_add_job($job_name, 'tripal_annotation_history', 'tripal_annotation_history_clear_tables', array($format_array_clear), $GLOBALS['user']->uid);
  
  // When Install / Uninstall button was clicked
  } else if ($form_state['clicked_button']['#id'] === 'install-submit') {
  
    $results = tripal_annotation_history_get_audit_tables();
  
    $format_array_create = array();
    $format_array_delete = array();
    foreach ($form_state['input']['table'] as $table_name => $value) {
    
      if (is_null($value) && isset($results[$table_name . tripal_annotation_history_table_suffix()])) {
        $format_array_delete[] = $table_name;
      } 
      if (!is_null($value) && !isset($results[$table_name . tripal_annotation_history_table_suffix()])) {
        $format_array_create[] = $table_name;
      }
    
    }
    
    // Adds of update operation to the Tripal CRON
    $job_name = t("CC Annotation History : Update tables (INSTALL : " . implode(", ", $format_array_create) . "; UNINSTALL : " . implode(", ", $format_array_delete) . ")");
    tripal_add_job($job_name, 'tripal_annotation_history', 'tripal_annotation_history_update_tables', array($format_array_create, $format_array_delete), $GLOBALS['user']->uid);
    
  } 
  
  if (!(isset($_GET['page']) || isset($_GET['chado-name']) 
    || isset($_GET['records-order']) || isset($_GET['records-number']))) {
    // Redirection to the administration page of the database
    drupal_goto('admin/tripal/extension/tripal_annotation_history/install/index', array('query' => array('advanced' => 'true')));
  }
  
}
 
 
 