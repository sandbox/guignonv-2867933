<?php

/**
 * Implements hook_form().
 *
 * The form contains the list of all protection sets.
 * All rows have a checkbox to enable or disable the protection set. 
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_form() {
  $results = tripal_access_restriction_get_protection_sets();
  
  // Converts the SQL result into arrays for the Drupal form module
  // Array which contains data about the row
  $table_data['data'] = array();
  // Array which contains data to know if row needs to be checked in the list
  $table_data['check'] = array();
  foreach ($results as $result) {
    $result->additional_tables = preg_replace('/^\{(.*)\}$/', '${1}', $result->additional_tables);
    
    $nb_records = chado_query('SELECT COUNT(1) FROM {' . $result->main_table . '};')->fetchField();
    $nb_p_records = chado_query('SELECT COUNT(1) FROM {chaco_' . $result->main_table . '_access} WHERE chaco_user_id = 0 AND access_level > 0;')->fetchField();
    $nb_no_access = chado_query('SELECT COUNT(1) FROM {' . $result->main_table . '} WHERE ' . $result->key . ' NOT IN (SELECT ' . $result->key . ' FROM {chaco_' . $result->main_table . '_access});')->fetchField();
    
    $table_data['data'][$result->main_table] = array(
      'name' => t('ChaCo ' . ucfirst(str_replace('_' , ' ', $result->main_table))),
      'chado_tables' => '<b>' . $result->main_table . '</b>,' . $result->additional_tables,
      'records' => $nb_records,
      'precords' => $nb_p_records,
      'no_access' => $nb_no_access,
      'access' => l(t('Manage'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/manage')
        . '<br>' . l(t('All public'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/public', array('attributes' => array('title' => t('Read access'))))
        . '<br>' . l(t('Clear'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/clear'),
      'manage' => l(t('Edit set'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/edit')
        . '<br>' . l(t('Export'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/export')
        . '<br>' . l(t('Remove'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $result->main_table . '/remove'),
    );
    $table_data['check'][$result->main_table] = $result->enabled;
  }
  
  $form = array();
  // Generates the list of protections sets
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => array(
      'name' => t('Name'),
      'chado_tables' => array(
        'data' => t('Chado Tables'),
        'class' => 'tripal_access_restriction_chado_tables_col',
      ), 
      'records' => t('Records'),
      'precords' => t('Public Records'),
      'no_access' => t('No access set'),
      'access' => t('Access'),
      'manage' => t('Manage'), 
    ),
    '#options' => $table_data['data'],
    '#empty' => t('No protection set found'),
    '#prefix' => '<h1>' . t('Protection Sets') . '</h1>
      <ul class="action-links">
        <li>' . l(t('Add new protection set'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/add') . '</li>
        <li>' . l(t('Import protection set'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/import') . '</li>
      </ul>'
    ,
    '#default_value' => $table_data['check'],
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'update-submit',
    '#value' => t('Update status'),
    
  );
  
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_form_submit($form, &$form_state) {
  $protection_sets = tripal_access_restriction_get_protection_sets();
  $protection_sets_array = array();
  foreach ($protection_sets as $protection_set) {
    $protection_sets_array[$protection_set->main_table] = $protection_set->main_table;
  }
  
  $protection_sets_to_disabled = array_diff_assoc($protection_sets_array, $form_state['values']['table']);
  foreach($protection_sets_to_disabled as $protection_set_to_disabled) {
    $data = (array) tripal_access_restriction_get_protection_set_data($protection_set_to_disabled);
    if ($data['enabled']) {
      $data['enabled'] = false;
      tripal_access_restriction_update_protection_set($data, false);
    }
  }
  
  foreach ($form_state['values']['table'] as $table) {
  
    if (empty($table)) {
      continue;
    }
    
    $data = (array) tripal_access_restriction_get_protection_set_data($table);  
    if (!$data['enabled']) {    
      $data['enabled'] = true;
      tripal_access_restriction_update_protection_set($data, false);
    }
  }
  
  tripal_access_restriction_repair_access_restriction();
}

/**
 * Implements hook_form().
 *
 * Form to add or update a protection set.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_update_form($form, &$form_state, $op = 'add', $main_table_name = NULL) {
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Chado Controller Access Restriction'), 'admin/tripal/extension/tripal_access_restriction');
  $crumbs[] = l(t('Protection Sets'), 'admin/tripal/extension/tripal_access_restriction/protection_sets');
  drupal_set_breadcrumb($crumbs);
  
  if ($op == 'edit' && !empty($main_table_name)) {
    $data = tripal_access_restriction_get_protection_set_data($main_table_name);
    if (empty($data) || isset($form_state['values']['key'])) {
      unset($data);
      $op = 'add';
    } else {
      $data->additional_tables = tripal_access_restriction_convert_to_php_array($data->additional_tables);
      $data->insert_access = preg_replace('/^\{(.*)\}$/', '${1}', $data->insert_access);
    }
  } else {
    $op = 'add';
  }
  
  $form = array(
    '#attributes' => array('class' => 'tripal_access_restriction_protection_sets_exposed_form'),
  );
  
  // Information message if JS is disabled
  $form['no_js'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'style' => 'display:block; margin-bottom: 1em;',
      'class' => array('messages warning'),
    ),
    '#prefix' => '<noscript>',
    '#suffix' => '</noscript>',
  );
  $form['no_js']['text'] = array(
    '#markup' => t('This form has a better look with JavaScript on. You can activate JavaScript in your browser settings.'),
  );
  
  // MAIN TABLE FIELD
  $schema_name = tripal_access_restriction_chado_schema_name();
  $table_type = 'BASE TABLE';
  $sql = 'SELECT * FROM {information_schema.tables}';
  $sql .= ' WHERE table_schema = :schema_name AND table_type = :table_type AND table_name NOT LIKE \'chaco_%\' ORDER BY table_name;';
  $select = db_query($sql, array('schema_name' => $schema_name, 'table_type' => $table_type));
  $results = $select->fetchAll();
  $options = array(0 => t('No table selected'));
  foreach ($results as $result) {
    if (!tripal_access_restriction_protection_set_exists($result->table_name)) {
      $options[$result->table_name] = $result->table_name;
    }
  }
  if (isset($data)) {
    $options[$data->main_table] = $data->main_table;
    ksort($options);
  }
  $form['main_table'] = array(
    '#type' => 'select',
    '#title' => t('Main table') . '<span class="form-required" title="This field is required.">*</span>',
    '#options' => $options,
    '#ajax' => array(
      'callback' => 'tripal_access_restriction_protection_sets_update_form_ajax',
      'wrapper' => 'tripal_access_restriction_ajax',
    ),
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['main_table']['#default_value'] = $data->main_table;
    $form_state['values']['main_table'] = $data->main_table;
    $form['main_table']['#disabled'] = true;
  }
  
  // Button is display if JS is disabled
  $form['refresh-table'] = array(
    '#type' => 'button',
    '#value' => t('Validate table'),
    '#prefix' => '<noscript>',
    '#suffix' => '</noscript>',
  );

  // KEY FIELD
  $options = array(0 => t('No key selected'));
  if (!empty($form_state['values']['main_table'])) {
    $table = $form_state['values']['main_table'];
    if (isset($data)) {
      $table = tripal_access_restriction_get_table_prefix() . $table;
    }
    $sql = 'SELECT ikcu.column_name FROM {information_schema.table_constraints} itc';
    $sql .= ' JOIN {information_schema.key_column_usage} ikcu using(constraint_name)';
    $sql .= ' WHERE (itc.constraint_type = \'PRIMARY KEY\' OR itc.constraint_type = \'UNIQUE\') AND ikcu.table_name = itc.table_name AND itc.table_name = :table_name AND ikcu.table_schema = itc.table_schema AND itc.table_schema = :schema_name';
	$sql .= ' AND NOT EXISTS (SELECT 1 FROM information_schema.key_column_usage ikcu2 WHERE ikcu2.column_name != ikcu.column_name AND ikcu2.table_schema = ikcu.table_schema AND ikcu2.constraint_name = ikcu.constraint_name)';
    $sql .= ' ORDER BY itc.constraint_type ASC, ikcu.column_name ASC;';
    $select = db_query($sql, array('schema_name' =>  $schema_name, 'table_name' => $table));
    $results = $select->fetchAll();
    foreach ($results as $result) {
      $options[$result->column_name] = $result->column_name;
    }
  }
  if (isset($data)) {
    $options[$data->key] = $data->key;
    ksort($options);
  }
  $form['key'] = array(
    '#type' => 'select',
    '#title' => t('Key') . '<span class="form-required" title="This field is required.">*</span>',
    '#options' => $options,
    '#prefix' => '<div id="tripal_access_restriction_ajax">',
    '#ajax' => array(
      'callback' => 'tripal_access_restriction_protection_sets_update_form_ajax',
      'wrapper' => 'tripal_access_restriction_ajax',
    ),
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['key']['#default_value'] = $data->key;
    $form_state['values']['key'] = $data->key;
    $form['key']['#disabled'] = true;
  }
  
  // Button is display if JS is disabled
  $form['refresh-key'] = array(
    '#type' => 'button',
    '#value' => t('Validate key'),
    '#prefix' => '<noscript>',
    '#suffix' => '</noscript>',
  );
  
  // ADDITIONAL TABLES FIELD
  $options = array(0 => t('No additional table found.'));
  if (!empty($form_state['values']['key'])) {
    $table = $form_state['values']['main_table'];
    $key = $form_state['values']['key'];
    $options = array();
    $additional_tables = tripal_access_restriction_get_additionnal_tables($table, $key);
    foreach ($additional_tables as $additional_table) {
	  $table_not_protected = !tripal_access_restriction_protection_set_exists($additional_table['table_name']);
	  $protected_by_p_set = isset($data) && in_array($additional_table['table_name'], $data->additional_tables);
      if ($table_not_protected || $protected_by_p_set) {
        $options[$additional_table['table_name']] = $table . ' (' . $key . ') => ' . $additional_table['table_name'] . '(' . $additional_table['table_key'] . ')';
      }
    }
  }
  $form['additional_tables'] = array(
    '#type' => 'select',
    '#multiple' => true,
    '#size' => 10,
    '#title' => t('Additional tables'),
    '#description' => t('Hold down the Ctrl (Windows) / Command (Mac) button to select multiple options.'),
    '#options' => $options,
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['additional_tables']['#default_value'] = $data->additional_tables;
    $form_state['values']['additional_tables'] = $data->additional_tables;
  }
  
  // MANAGEMENT VIEW FIELD
  $options = array(0 => t('No view found.'));
  if (isset($key)) {
    $options = array(0 => t('No view selected.'));
    $views = views_get_all_views();
    foreach ($views as $view) {
      if (array_key_exists($key, $view->get_items('field'))) {
        $options[$view->name] = $view->human_name;
      }
    }
  }
  $form['management_view'] = array(
    '#type' => 'select',
    '#title' => t('Management view'),
    '#options' => $options,
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['management_view']['#default_value'] = $data->management_view;
    $form_state['values']['management_view'] = $data->management_view;
  }
  
  // INHERIT ACCESS FIELD
  $default = '';
  if (isset($key)) {
    $default = 'SELECT NEW.' . $key . ', cu.chado_user_id, 3 FROM {chaco_user} cu WHERE cu.name = session_user UNION SELECT NEW.' . $key . ', 1, 3;';
  }
  $form['inherit_access'] = array(
    '#type' => 'textfield',
    '#title' => t('New elements inherit access from (SQL Query)'),
    '#value' => $default,
    '#maxlength' => 255,
    '#suffix' => '</div>',
    '#disabled' => true,
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['inherit_access']['#default_value'] = $data->inherit_access;
    $form_state['values']['inherit_access'] = $data->inherit_access;
  }
  
  // ENABLED FIELD
  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
  );
  if (isset($data)) {
    $form['enabled']['#default_value'] = $data->enabled;
    $form_state['values']['enabled'] = $data->enabled;
  }
  
  // INSERT ACCESS FIELD
  $form['insert_access'] = array(
    '#type' => 'textfield',
    '#title' => t('Insert access'),
    '#description' => 'Separate user name by a coma. Ex: toto,titi,tutu',
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['insert_access']['#default_value'] = $data->insert_access;
    $form_state['values']['insert_access'] = $data->insert_access;
  }
  
  // COMMENTS FIELD
  $form['comments'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative comments'),
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  if (isset($data)) {
    $form['comments']['#default_value'] = $data->comments;
    $form_state['values']['comments'] = $data->comments;
  }
  
  // SUBMIT BUTTONS
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save protection set'),
    '#id' => $op . '-submit',
  );
  if ($op != 'add') {
    $form['export'] = array(
      '#type' => 'submit',
      '#value' => t('Export'),
      '#id' => 'export-submit',
    );
    
    $form['remove'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#id' => 'remove-submit',
    );
  }
  
  return $form;
}

/**
 * Ajax callback function use by "tripal_access_restriction_protection_sets_update_form".
 *
 * Rebuild some fields in Ajax to have a dynamic form.
 *
 * @return
 *   Fields updated
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_update_form_ajax($form, &$form_state) {
  return drupal_render($form['key']) . 
    drupal_render($form['additional_tables']) . 
    drupal_render($form['management_view']) .
    drupal_render($form['inherit_access']);
}

/**
 * Implements hook_form_validate().
 *
 * Validate callback function of the form "tripal_access_restriction_protection_sets_update_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_update_form_validate($form, &$form_state) {
  if (empty($form_state['values']['main_table'])) {
    form_set_error('main_table', 'Main Table must be defined.');
  }
  
  if (empty($form_state['values']['key'])) {
    form_set_error('key', 'Key must be defined.');
  }
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_update_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_update_form_submit($form, &$form_state) {  
  // Convert HTML / PHP value in postgreSQL value
  if ($form_state['values']['management_view'] === '0') {
    $form_state['values']['management_view'] = '';
  }
  $form_state['values']['additional_tables'] = tripal_access_restriction_convert_to_postgre_array($form_state['values']['additional_tables']);
  $form_state['values']['insert_access'] = '{' . $form_state['values']['insert_access'] . '}';
  
  // Add action
  if ($form_state['clicked_button']['#id'] === 'add-submit') {
    if (tripal_access_restriction_add_protection_set($form_state['values'])) {
      $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
    }
  }
  
  // Edit action
  if ($form_state['clicked_button']['#id'] === 'edit-submit') {
    tripal_access_restriction_update_protection_set($form_state['values']);
  }
  
  // Export action
  if ($form_state['clicked_button']['#id'] === 'export-submit') {
    $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets/' .  $form_state['values']['main_table'] . '/export';
  }
  
  // Remove action
  if ($form_state['clicked_button']['#id'] === 'remove-submit') {
    $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets/' .  $form_state['values']['main_table'] . '/remove';
  }
}

/**
 * Implements hook_form().
 *
 * Form to export a protection set.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_export_form($form, &$form_state, $main_table_name = NULL) {
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Chado Controller Access Restriction'), 'admin/tripal/extension/tripal_access_restriction');
  $crumbs[] = l(t('Protection Sets'), 'admin/tripal/extension/tripal_access_restriction/protection_sets');
  drupal_set_breadcrumb($crumbs);
  
  // Get protection set data depending to the id passed through the URL
  if (!empty($main_table_name)) {
    $data = tripal_access_restriction_get_protection_set_data($main_table_name);
    if (empty($data)) {
      drupal_set_message(t('No data found'), 'error');
      $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
      return $form;
    }
  }
  
  $data = (array)$data;
  // Remove the id field of the export array
  unset($data['chaco_access_config_id']);
  
  // Field which contains the array
  $form['export'] = array(
    '#title' => t('Export ' . $main_table_name . ' protection set'),
    '#type' => 'textarea',
    '#default_value' =>  '$protection_set = ' . var_export($data, true) . ';',
    '#rows' => 25,
  );
  
  return $form;
}

/**
 * Implements hook_form().
 *
 * Form to import a protection set.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_import_form($form, &$form_state) {  
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Chado Controller Access Restriction'), 'admin/tripal/extension/tripal_access_restriction');
  $crumbs[] = l(t('Protection Sets'), 'admin/tripal/extension/tripal_access_restriction/protection_sets');
  drupal_set_breadcrumb($crumbs);

  $form['import'] = array(
    '#title' => t('Import new protection set'),
    '#type' => 'textarea',
    '#description' => t('Paste here the protection set export.'),
    '#rows' => 25,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#id' => 'import-submit',
  );
  
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_import_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_import_form_submit($form, &$form_state) {
  eval($form_state['values']['import']);
  if (isset($protection_set)) {
    if (tripal_access_restriction_add_protection_set($protection_set)) {
      $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
    } else {
      form_set_error('import');
    }
  } else {
    form_set_error('import', '$protection_set must be defined.');
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form().
 *
 * Confirmation form when a user wants to delete a protection set.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_remove_form($form, &$form_state, $main_table_name) {
  $form_state['protection_set'] = $main_table_name;
  return confirm_form($form,
    t('Are you sure you want to delete this protection set ?'),
    'admin/tripal/extension/tripal_access_restriction/protection_sets',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_remove_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_remove_form_submit($form, &$form_state) {
  tripal_access_restriction_remove_protection_set($form_state['protection_set']);
  $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
}

/**
 * Implements hook_form().
 *
 * Form to manage a protection set.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_manage_form($form, &$form_state, $main_table_name = NULL) {
  $data = tripal_access_restriction_get_protection_set_data($main_table_name);
  if (empty($data)) {
    drupal_set_message(t('No data found'), 'error');
    $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
    
    return $form;
  }
  
  // If management view is missing we display a warning message
  if (empty($data->management_view)) {  
    $form['management_view_missing'] = array(
      '#type' => 'container',
      '#prefix' => t('Management view is missing. Please define one ')
        . l(t('here'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $data->main_table . '/edit') . ' ' . t('(fill the \'Management view\' field).') . ' 
        <br> <i>' . t('If the field is empty, create a view in \'Structure > Views\' menu which uses the protected key.') . '</i>',
      '#attributes' => array('style' => 'display:block; margin-bottom: 1em;')
    );
    
    return $form;
  }
      
  $form = array(
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_exposed_form')),
  );
  
  // Main table of the protection set
  $form['main_table'] = array(
    '#type' => 'container',
    '#prefix' => '<b>' . t('Table: ') . '</b>' . $data->main_table . ' (' 
      . l(t('settings'), 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $data->main_table . '/edit') . ')',
    '#attributes' => array('style' => 'display:block; margin-bottom: 1em;')
  );
  
  // Additional tables in the protection set
  $additional_tables = tripal_access_restriction_convert_to_php_array($data->additional_tables);
  if (empty($additional_tables)) {
    $additional_tables = array (0 => '<i>none</i>');
  }
  $form['additional_tables'] = array(
    '#type' => 'container',
    '#prefix' => '<b>' . t('Linked tables: ') . '</b>' . implode(', ', $additional_tables),
    '#attributes' => array('style' => 'display:block; margin-bottom: 1em;')
  );
  
  // VIEW BLOCK
  // Display the management view with check boxes
  $view = views_get_view($data->management_view, 'default');
  $tmp[$data->key . '-checkbox'] = array(
       'id'    => 'php',
       'table' => 'views',
       'field' => 'php',
     'label' => 'Manage',
     'use_php_setup' => 0,
     'php_output' => '<input type="checkbox" name="<?php echo \'tripal-access-config-id[\' . $row->' . $data->key . ' . \']\'; ?>" value="<?php echo $row->' . $data->key . '; ?>">',
     'use_php_click_sortable' => '0',
     'php_click_sortable' => '',
  );
  $view->display['default']->display_options['fields'] = $tmp + $view->display['default']->display_options['fields'];
  
  $form['admin-view'] = array(
    '#type' => 'container',
    '#prefix' => '<b>' . t('View : ') . '</b>' . $view->human_name, 
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_exposed_form')),
  );
  
  $form['admin-view']['view-block'] = array(
    '#type' => 'markup',
    '#markup' => $view->preview(),
  );
  
  // Clear submit
  $form['clear-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear access'),
    '#id' => 'clear-submit',
  );
  
  // Public read submit
  $form['pread-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set public read'),
    '#id' => 'pread-submit',
  );
  
  // Public write submit
  $form['pwrite-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set public write'),
    '#id' => 'pwrite-submit',
  );
  
  // ADVANCED FORM
  $form['advanced'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Advanced'), 
    '#collapsible' => TRUE, 
    '#collapsed' => TRUE,
  );
  
  // Group level access
  $form['advanced']['group'] = array(
    '#type' => 'container',
    '#prefix' => '<h3>' . t('Group-level access') . '</h3>', 
  );
  $header = array(
    'name' => t('Name'), 
    'none' => t('None'), 
    'read' => t('Read'), 
    'write' => t('Write')
  );
  $rows = array();
  $groups = tripal_access_restriction_get_groups();
  foreach ($groups as $group) {
    $rows[$group->chaco_user_id] = array(
      'name' => $group->name,
      'none' => '<input type="radio" name="tripal-access-config-advanced[' . $group->chaco_user_id . ']" value="0">',
      'read' => '<input type="radio" name="tripal-access-config-advanced[' . $group->chaco_user_id . ']" value="1">',
      'write' => '<input type="radio" name="tripal-access-config-advanced[' . $group->chaco_user_id . ']" value="3">',
    );
  }
  $form['advanced']['group']['group-table'] = array(
    '#type' => 'markup', 
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No categories available.'))),
  );
  
  // User-level access
  $form['advanced']['user'] = array(
    '#type' => 'container',
    '#prefix' => '<h3>' . t('User-level access') . '</h3>', 
  );
  $header = array(
    'name' => t('Name'), 
    'none' => t('None'), 
    'read' => t('Read'), 
    'write' => t('Write')
  );
  $rows = array();
  $users = tripal_access_restriction_get_users();
  foreach ($users as $user) {
    $rows[$user->chaco_user_id] = array(
      'name' => $user->name,
      'none' => '<input type="radio" name="tripal-access-config-advanced[' . $user->chaco_user_id . ']" value="0">',
      'read' => '<input type="radio" name="tripal-access-config-advanced[' . $user->chaco_user_id . ']" value="1">',
      'write' => '<input type="radio" name="tripal-access-config-advanced[' . $user->chaco_user_id . ']" value="3">',
    );
  }
  $form['advanced']['user']['user-table'] = array(
    '#type' => 'markup', 
    '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No categories available.'))),
  );
  
    
  $form['advanced']['update-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update access'),
    '#id' => 'update-submit',
  );
  
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_manage_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_manage_form_submit($form, &$form_state) {
  if (isset($form_state['input']['tripal-access-config-id'])) {
    $selected_values = $form_state['input']['tripal-access-config-id'];
  } else {
    $selected_values = array();
  }
  
  $main_table = $form_state['build_info']['args'][0];
  $data = tripal_access_restriction_get_protection_set_data($main_table);
  
  $op = $form_state['clicked_button']['#id'];
  // var_dump($form_state); die();
  if ($op === 'clear-submit') {
    if (empty($selected_values)) {
      drupal_set_message('No row selected.', 'error');
    } else {
      $sql = 'DELETE FROM {chaco_' . $main_table . '_access} WHERE ' . $data->key . ' = :id;';
      
      foreach ($selected_values as $value) {
        chado_query($sql, array(
          'id' => $value,
        ));
      }
      
      drupal_set_message('Access cleared.');
    }
  } 
  if($op === 'pread-submit') {
    $form_state['input']['tripal-access-config-advanced'] = array();
    $form_state['input']['tripal-access-config-advanced'][0] = 1;
    $op =  'update-submit';
  }
  if($op === 'pwrite-submit') {
    $form_state['input']['tripal-access-config-advanced'] = array();
    $form_state['input']['tripal-access-config-advanced'][0] = 3;
    $op =  'update-submit';
  } 
  if($op === 'update-submit') {
    if (empty($selected_values)) {
      drupal_set_message('No row selected.', 'error');
    } else {
      
      if (isset($form_state['input']['tripal-access-config-advanced'])) {
        $rights = $form_state['input']['tripal-access-config-advanced'];
      } else {
        $rights = array();
      }
      
      foreach ($selected_values as $value) {
        foreach ($rights as $user_id => $access_level) {
          tripal_access_restriction_set_user_right($main_table, $value, $user_id, $access_level);
        }
      }
      
      drupal_set_message('Configuration saved.');
    }
  }
  
  $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets/' . $main_table . '/manage';
}

/**
 * Implements hook_form().
 *
 * Confirmation form when a user wants to give public access for a protection set.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_public_form($form, &$form_state, $main_table) {
  $form_state['main_table'] = $main_table;
  return confirm_form($form,
    t('Are you sure you want to give public access for this table ?'),
    'admin/tripal/extension/tripal_access_restriction/protection_sets',
    t('This action cannot be undone.'),
    t('Give public access for everyone'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_public_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_public_form_submit($form, &$form_state) {
  $data = tripal_access_restriction_get_protection_set_data($form_state['main_table']);
  $sql = 'DELETE FROM {chaco_' . $data->main_table . '_access} WHERE chaco_user_id = 0;
      INSERT INTO {chaco_' . $data->main_table  . '_access} SELECT ' . $data->key . ', 0, 1, \'TODO\' FROM {' . $data->main_table . '};';
  chado_query($sql);
  drupal_set_message('Public access given for everyone.');
  $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
}

/**
 * Implements hook_form().
 *
 * Confirmation form when a user wants to clear access for a protection set.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_clear_form($form, &$form_state, $main_table) {
  $form_state['main_table'] = $main_table;
  return confirm_form($form,
    t('Are you sure you want to clear access for this table ?'),
    'admin/tripal/extension/tripal_access_restriction/protection_sets',
    t('This action cannot be undone.'),
    t('Clear access'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_protection_sets_clear_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_sets_clear_form_submit($form, &$form_state) {
  $sql = 'TRUNCATE {chaco_' . $form_state['main_table'] . '_access};';
  chado_query($sql);
  drupal_set_message('Access cleared.');
  $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/protection_sets';
}