<?php

// Protection set admin pages and forms
require_once 'tripal_access_restriction.p_set.admin.inc';

// Group admin pages and forms
require_once 'tripal_access_restriction.group.admin.inc';

// User admin pages and forms
require_once 'tripal_access_restriction.user.admin.inc';


/**
 * Remove access of one ChaCo user or group from all protected tables.
 *
 * @param $user_id
 *  Id of one ChaCo user or one ChaCo group.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_users_clear_access($user_id, $src = NULL) {
  $protection_sets = tripal_access_restriction_get_protection_sets();
  foreach ($protection_sets as $protection_set) {
	$query = 'DELETE FROM chaco_' . $protection_set->main_table . '_access WHERE chaco_user_id = :user_id;';
    $params = array(
      'user_id' => $user_id,
    );
	chado_query($query, $params);
  }
  
  if (!empty($src)) {
    drupal_set_message('Access cleared.');
    drupal_goto('admin/tripal/extension/tripal_access_restriction/' . $src);
  }
}

/**
 * Implements hook_form().
 *
 * Form to parameter the module.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_parameters_form() {  
  $form = array(
    '#attributes' => array('class' => 'tripal_access_restriction_protection_sets_exposed_form'),
  );
  
  // Chaco Group Name in the database
  $chaco_group = tripal_access_restriction_get_chaco_group();
  $form['chaco_group'] = array(
    '#type' => 'textfield',
    '#title' => t('ChaCo Group (in database)'),
    '#disabled' => true,
    '#default_value' => $chaco_group,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#id' => 'edit-submit',
  );
  
  return $form;
}

