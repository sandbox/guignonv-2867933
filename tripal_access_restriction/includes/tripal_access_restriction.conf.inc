<?php

/*
 * Return the name of the table where are saved the protection sets.
 *
 * @return
 *   The name of the table.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_protection_set_config_table() {
  return 'chaco_access_config';
}

/*
 * Drupal database admin account.
 *
 * @return
 *   The account name.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_admin_account() {
  global $databases;
  return $databases['default']['default']['username'];
}

/*
 * Database flag which indicates that the row is a group and not a user.
 *
 * @return
 *   The flag.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_flag_group() {
  return 0b0000000000000001;
}

/*
 * Database flag which indicates that the row is a disabled group or a disabled user.
 *
 * @return
 *   The flag.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_flag_disabled() {
  return 0b0000000000000010;
}

/*
 * Database flag which indicates that the row is an administrator group 
 * or an administrator user.
 *
 * @return
 *   The flag.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_flag_admin() {
  return 0b0000000000000100;
}

/*
 * Return the name of the current chado schema.
 *
 * @return
 *   The name of the current chado schema.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_chado_schema_name() {
  return isset($GLOBALS['chado_active_db']) ? $GLOBALS['chado_active_db'] : 'chado';
}

/*
 * Return the prefix of protected tables.
 *
 * @return
 *   The prefix of protected tables.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_table_prefix() {
  return 'chaco_p_';
}

/*
 * Return the group name of chaco users in database.
 *
 * @return
 *   The group name.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_chaco_group() {
  return chado_query('SELECT {chaco_get_chaco_group_name()};')->fetchField();
}