<?php 

require_once 'tripal_access_restriction.conf.inc';

/**
 * Get data about a protection set.
 *
 * @param $main_table_name
 *  name of the main table of one protection set.
 *
 * @return 
 *  An object which represent one protection set.
 *  Attributes are the name of columns of the chaco_access_config table.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_protection_set_data($main_table_name) {
  $config_table = tripal_access_restriction_get_protection_set_config_table();
  $sql = 'SELECT * FROM {' . $config_table . '} WHERE main_table = :main_table';
  $select = chado_query($sql, array('main_table' => $main_table_name));
  return $select->fetch();
}

/**
 *  Get all tables which have relation with the $main_table using the $key. 
 *  This function uses the integrated view system of Tripal, so maybe 
 *  not all tables will be returned.
 *
 * @param $main_table
 *  name of the table
  * @param $key
 *  name of the key
 *
 * @return 
 *  An array which contains all tables.
 *  array (
 *    'table_name1' => array (
 *      'table_name' => 'table_name1',
 *      'table_key' => 'table_key1',
 *    ),
 *    ...
 *  );
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_additionnal_tables($main_table, $key) {
  $result = array();
  $setup_id = tripal_get_lightest_views_integration_setup($main_table);
  if ($setup_id) {
    $defn_array = tripal_export_views_integration($setup_id);
    
    if (!isset($defn_array['fields'][$key])) {
      return $result;
    }
    
    foreach ($defn_array['fields'][$key]['joins'] as $joins) {
      foreach ($joins as $left_field => $join) {
        $result[$join['table']] = array('table_name' => $join['table'], 'table_key' => $left_field);
      }
    }
  }
  return $result;
}

/**
 * Tests if a protection set exists using a main table name as key.
 *
 * @param $table_name
 *  name of the main table of one protection set or array contains names of table.
 *  if it is an array, $table_name need to look like this :
 *  array(0 => 'table_name1', 1 => 'table_name2', ...);
 *
 * @return 
 *   true if a protection set already exists
 *  false if a protection set doesn't exist
 *  if $table_name is an array return the name of the first table which has a protection set instead of true
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protection_set_exists($table_name) {
  if (is_array($table_name)) {
    foreach ($table_name as $name) {
      if (tripal_access_restriction_protection_set_exists($name)) {
        return $name;
      }
    }
  } else {
    $config_table = tripal_access_restriction_get_protection_set_config_table();
    
    // Check if the table is a main table
    $data = tripal_access_restriction_get_protection_set_data($table_name);
    if (!empty($data)) {
      return true;
    }
    
    // Check if the table is in any additional tables
    $sql = 'SELECT 1 FROM {' . $config_table . '} WHERE :table_name = ANY(additional_tables) LIMIT 1';
    $result = db_query($sql, array('table_name' => $table_name))->fetchField();
    if (!empty($result)) {
      return true;
    }
  }
  
  return false;
}

/**
 * Tests if the protected table corresponding to the table name exist.
 * (protected table = chaco_p_$table_name)
 *
 * @param $table_name
 *  name of the table
 *
 * @return 
 *   true if the protected table exists
 *  false if the protected table doesn't exist
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_protected_table_exists($table_name) {
  $table = db_query('SELECT 1 FROM {information_schema.tables} WHERE  table_schema = :schema_name AND table_name = :table_name AND table_type = \'BASE TABLE\'', 
      array('schema_name' => tripal_access_restriction_chado_schema_name(), 'table_name' => tripal_access_restriction_get_table_prefix() . $table_name)
  )->fetchField();
  return !empty($table);
}

/**
 * Convert a postgreSQL array to a PHP array.
 *
 * @param $postgre_array
 *  postgreSQL array as string (ex: "{'elem1', 'elem2', 'elem3'}")
 *
 * @return 
 *  PHP array (array(0 => 'elem1', 1 => 'elem2', 2 => 'elem3'))
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_convert_to_php_array($postgre_array) {
  $php_array = preg_replace('/^\{(.*)\}$/', '${1}', $postgre_array);
  
  if (empty($php_array)) {
    return array();
  }
  
  $php_array = explode(',', $php_array);
  return $php_array;
}

/**
 * Convert a PHP array to a postgreSQL array.
 *
 * @param $php_array
 *   PHP array (array(0 => 'elem1', 1 => 'elem2', 2 => 'elem3'))
 *
 * @return 
 *  postgreSQL array as string (ex: "{'elem1', 'elem2', 'elem3'}")
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_convert_to_postgre_array($php_array) {
  $postgre_array = '{' . implode(',', $php_array) . '}';
  return $postgre_array;
}

/**
 * Returns the list of all ChaCo groups.
 * If user id is specified returns the list of Chaco groups of the user.
 *
 * @param $user_id
 *  Id of a ChaCo user.
 *
 * @return 
 *  A list of ChaCo group.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_groups($user_id = NULL) {
  if (is_null($user_id)) {
    $sql = 'SELECT * FROM {chaco_user}';
    $sql .= ' WHERE (flags & ' . tripal_access_restriction_get_flag_group() . ') != 0';
    $sql .= ' ORDER BY chaco_user_id DESC;'; 
    return chado_query($sql)->fetchAll();
  } else {
    $sql = 'SELECT * FROM {chaco_user_group} JOIN {chaco_user} ON group_id = chaco_user_id WHERE user_id = :user_id';
    return chado_query($sql, array('user_id' => $user_id))->fetchAll();
  }
}

/**
 * Returns the list of all ChaCo users.
 * If group id is specified returns the list of Chaco users in the group.
 *
 * @param $user_id
 *  Id of a ChaCo group.
 *
 * @return 
 *  A list of ChaCo user.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_users($group_id = NULL) {
  if (is_null($group_id)) {
    $sql = 'SELECT * FROM {chaco_user}';
    $sql .= ' WHERE (flags & ' . tripal_access_restriction_get_flag_group() . ') = 0';
    $sql .= ' ORDER BY chaco_user_id DESC;'; 
    return chado_query($sql)->fetchAll();  
  } else {
    $sql = 'SELECT * FROM {chaco_user_group} JOIN {chaco_user} ON user_id = chaco_user_id WHERE group_id = :group_id';
    return chado_query($sql, array('group_id' => $group_id))->fetchAll();
  }
}

/**
 * Add a ChaCo user in a ChaCo group.
 *
 * @param $user_id
 *  Id of a ChaCo user.
 * @param $group_id
 *  Id of a ChaCo group.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_add_user_to_group($user_id, $group_id) {
  // Delete the old record if exists to avoid conflict with primary key.
  tripal_access_restriction_remove_user_from_group($user_id, $group_id);
  
  $sql = 'INSERT INTO {chaco_user_group} VALUES (:group_id, :user_id);';
  chado_query($sql, array(
    'group_id' => $group_id,
    'user_id' => $user_id,
  ));
}

/**
 * Remove a ChaCo user from a ChaCo group.
 *
 * @param $user_id
 *  Id of a ChaCo user.
 * @param $group_id
 *  Id of a ChaCo group.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_user_from_group($user_id, $group_id) {
  $sql = 'DELETE FROM {chaco_user_group} WHERE group_id = :group_id AND user_id = :user_id;';
  chado_query($sql, array(
    'group_id' => $group_id,
    'user_id' => $user_id,
  ));
}

/**
 * Returns the list of all protection sets.
 *
 * @return
 *  List of all protection sets. 
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_get_protection_sets() {
  $config_table = tripal_access_restriction_get_protection_set_config_table();
  $select = chado_query('SELECT * FROM {' . $config_table . '} ORDER BY chaco_access_config_id DESC');
  return $select->fetchAll();
}

/**
 * Add right for a user.
 *
 * @param $main_table
 *  Name of main table of a protection set.
 * @param $key_id
 *  Id of the same column than the key protected by the protection set of the main_table.
 * @param $user_id
 *  Id of a ChaCo user.
 * @param $access_level
 *  Access level (0 = none, 1 = read, 2 = update + 1, 3 = write + 2).
 * @param $comment
 *  Administrative comment for the row.
 *
 * @return
 *  True if the right has been created
 *  False if the right hasn't been created
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_set_user_right($main_table, $key_id, $user_id, $access_level = 0, $comment = 'TODO') {
  // Check if a protection set exists for the main main variable.
  $data = tripal_access_restriction_get_protection_set_data($main_table);
  if (empty($data)) {
    return false;
  }
  
  $sql = 'SELECT * FROM {chaco_' . $main_table . '_access} WHERE ' . $data->key . ' = :key_id AND chaco_user_id = :user_id;';
  $result = chado_query($sql, array(
    'key_id' => $key_id,
    'user_id' => $user_id,
  ))->fetch();
  
  // If a row with the user id and the key id already exists, we update rather than insert
  if (empty($result)) {
    $sql = 'INSERT INTO {chaco_' . $main_table . '_access} VALUES (:key_id, :user_id, :access_level, :comment);';
  } else {
    $sql = 'UPDATE {chaco_' . $main_table . '_access} SET ' . $data->key . ' = :key_id, chaco_user_id = :user_id, access_level = :access_level, comment = :comment WHERE ' . $data->key . ' = :key_id AND chaco_user_id = :user_id;';
  }
  
  // Execute the query
  chado_query($sql, array(
    'key_id' => $key_id,
    'user_id' => $user_id,
    'access_level' => $access_level,
    'comment' => $comment,
  ));
  
  return true;
}

/**
 * Repair access restriction module.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_repair_access_restriction() {
  $schema_name = tripal_access_restriction_chado_schema_name();
  chado_query('SELECT ' . $schema_name . '.chaco_repair_access_restriction();');
}
