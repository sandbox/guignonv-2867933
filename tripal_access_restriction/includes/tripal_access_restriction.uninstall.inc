<?php 

require_once 'tripal_access_restriction.conf.inc';
require_once 'tripal_access_restriction.util.inc';

/**
 * Remove a protection set.
 *
 * @param $main_table_name
 *  name of the main table of the protection set to remove
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set($main_table_name) {
  $config_table = tripal_access_restriction_get_protection_set_config_table();
  
  $transaction = db_transaction();
  try {    
    $data = tripal_access_restriction_get_protection_set_data($main_table_name);
    $additional_tables = tripal_access_restriction_convert_to_php_array($data->additional_tables);
  
    // Remove protection set from the chaco_access_config table
    $query = 'DELETE FROM {' . $config_table . '} WHERE main_table = :main_table;';
    chado_query($query, array('main_table' => $main_table_name));
    
    // Remove protection set from the main table
    tripal_access_restriction_remove_protection_set_rights($main_table_name);
    tripal_access_restriction_remove_protection_set_tables($main_table_name);
    tripal_access_restriction_remove_protection_set_functions($main_table_name);
    tripal_access_restriction_remove_protection_set_rules($main_table_name);
    tripal_access_restriction_remove_protection_set_rename_views($main_table_name);
    if (tripal_access_restriction_protected_table_exists($main_table_name))  {
      chado_query('DROP VIEW IF EXISTS {' . $main_table_name . '} CASCADE;');
      chado_query('ALTER TABLE {' . tripal_access_restriction_get_table_prefix() .  $main_table_name . '} RENAME TO ' . $main_table_name . ';');
    } else {
      chado_query('DROP VIEW IF EXISTS {' . tripal_access_restriction_get_table_prefix() . $main_table_name . '_view} CASCADE;');
    }
    
    // Remove protection set from all additional tables
    foreach ($additional_tables as $additional_table) {
      tripal_access_restriction_remove_protection_set_rights($additional_table);
      tripal_access_restriction_remove_protection_set_functions($additional_table);
      tripal_access_restriction_remove_protection_set_rules($additional_table);
      tripal_access_restriction_remove_protection_set_rename_views($additional_table);
      if (tripal_access_restriction_protected_table_exists($additional_table)) {
        chado_query('DROP VIEW IF EXISTS {' . $additional_table . '} CASCADE;');
        chado_query('ALTER TABLE {' . tripal_access_restriction_get_table_prefix() . $additional_table . '} RENAME TO ' . $additional_table . ';');
      } else {
        chado_query('DROP VIEW IF EXISTS {' . tripal_access_restriction_get_table_prefix() . $additional_table . '_view} CASCADE;');
      }
    }
    
    drupal_set_message('Protection sets removed');
  } catch (PDOException $e) {
    $transaction->rollback();
    drupal_set_message('Protection can\'t be removed : ' . $e->getMessage(), 'error');
  }
}

/**
 * Remove access tables of table in a protection set.
 *
 * @param $table_name
 *  name of the table which has the access tables to remove
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set_tables($table_name) {
  chado_query('DROP TABLE IF EXISTS {chaco_' . $table_name . '_access} CASCADE;');
  chado_query('DROP VIEW IF EXISTS {chaco_' . $table_name . '_access_max} CASCADE;');
}

/**
 * Remove functions of table in a protection set.
 *
 * @param $table_name
 *  name of the table which has functions to remove
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set_functions($table_name) {
  chado_query('DROP FUNCTION IF EXISTS {chaco_' . $table_name . '_assign_user_rights()} CASCADE;');
  if (tripal_access_restriction_protected_table_exists($table_name)) {
    chado_query('DROP TRIGGER IF EXISTS chaco_' . $table_name . '_assign_user_rights_i ON {' . tripal_access_restriction_get_table_prefix() .  $table_name . '} CASCADE;');
  }
  
  chado_query('DROP FUNCTION IF EXISTS {chaco_' . $table_name . '_has_write_access()} CASCADE;');
  chado_query('DROP FUNCTION IF EXISTS {chaco_' . $table_name . '_has_update_access(INTEGER)} CASCADE;');
  chado_query('DROP FUNCTION IF EXISTS {chaco_' . $table_name . '_has_delete_access(INTEGER)} CASCADE;');  
}

/**
 * Remove rules of table in a protection set.
 *
 * @param $table_name
 *  name of the table which has the rules to remove
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set_rules($table_name) {
  if (tripal_access_restriction_protected_table_exists($table_name)) {
    chado_query('DROP RULE IF EXISTS chaco_' . $table_name . '_insert ON {' . tripal_access_restriction_get_table_prefix() .  $table_name . '} CASCADE;');
    chado_query('DROP RULE IF EXISTS chaco_' . $table_name . '_update ON {' . tripal_access_restriction_get_table_prefix() .  $table_name . '} CASCADE;');
    chado_query('DROP RULE IF EXISTS chaco_' . $table_name . '_delete ON {' . tripal_access_restriction_get_table_prefix() .  $table_name . '} CASCADE;');
  }
}

/**
 * Rename all views which use a table in a protection set.
 *
 * @param $table_name
 *  name of the table used by the views
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set_rename_views($table_name) {
  $schema_name = tripal_access_restriction_chado_schema_name();
  
  // Function to rename views
  chado_query('CREATE OR REPLACE FUNCTION {temp_update_views()} RETURNS INTEGER AS
    $$
    DECLARE
      changed_views_count INTEGER;
      view_to_update RECORD;
      new_definition TEXT;
      changed_views_list TEXT;
    BEGIN
      changed_views_count := 0;
      changed_views_list := \'\';
      FOR view_to_update IN SELECT viewowner, viewname, definition  FROM pg_views WHERE schemaname = \'' . $schema_name . '\' AND viewname != \'' . $table_name . '[ ,;()]%\' LOOP
        new_definition := regexp_replace(view_to_update.definition, E\'([ ,\\\\(\\\\)])' . $table_name . '([ ,;\\\\(\\\\)])\', E\'\\\\1' . tripal_access_restriction_get_table_prefix() .  $table_name . '\\\\2\', \'g\');
        new_definition := regexp_replace(view_to_update.definition, E\'([ ,\\\\(\\\\)])' . $table_name . '_alias([ .,;\\\\(\\\\)])\', E\'\\\\1' . $table_name . '\\\\2\', \'g\');
        EXECUTE \'CREATE OR REPLACE VIEW \' || view_to_update.viewname || \' AS \' || new_definition || \';\';
        EXECUTE \'ALTER TABLE \' || view_to_update.viewname || \' OWNER TO \' || view_to_update.viewowner || \';\';
        changed_views_count := changed_views_count + 1;
        changed_views_list := changed_views_list || view_to_update.viewname;
      END LOOP;
      
      RAISE INFO \'The following views were updated: %\', changed_views_list;

      RETURN changed_views_count;
    END;
    $$
    LANGUAGE plpgsql;'
  );

  chado_query('SELECT {temp_update_views()} AS "Updated views";');
  chado_query('DROP FUNCTION IF EXISTS {temp_update_views()};');
}

/**
 * Remove rights of table in a protection set.
 *
 * @param $table_name
 *  name of the table which has the rights to remove
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_remove_protection_set_rights($table_name) {
  $schema_name = tripal_access_restriction_chado_schema_name();
  $group_name = tripal_access_restriction_get_chaco_group();
    
  if (tripal_access_restriction_protected_table_exists($table_name)) {
    chado_query('GRANT ALL ON TABLE {' . tripal_access_restriction_get_table_prefix() . $table_name . '} TO PUBLIC, GROUP ' . $group_name . ';');
  }  
    
  // prevent access to history (audit)
  chado_query('CREATE OR REPLACE FUNCTION {temp_grant_audit()} RETURNS BOOLEAN AS
    $$
    BEGIN
      BEGIN
        EXECUTE \'GRANT ALL ON TABLE ' . $schema_name . '.' . $table_name . '_audit TO GROUP \' || chaco_get_chaco_group_name() || \';\';
      EXCEPTION
        WHEN UNDEFINED_TABLE THEN
      END;
      RETURN TRUE;
    END;
    $$
    LANGUAGE plpgsql;'
  );
  chado_query('SELECT {temp_grant_audit()};');
  chado_query('DROP FUNCTION IF EXISTS {temp_grant_audit()};');
}

