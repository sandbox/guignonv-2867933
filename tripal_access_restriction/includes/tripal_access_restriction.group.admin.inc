<?php

/**
 * Main page to administrate the ChaCo groups.
 * List all groups and display some options.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_page() {
  // Action links
  $output = '<ul class="action-links"><li>' . 
    l(t('Add new group'), 'admin/tripal/extension/tripal_access_restriction/groups/add') 
	. '</li></ul>';
  
  // Get all groups
  $results = tripal_access_restriction_get_groups();
  
  // Create each rows
  $rows = array();
  foreach ($results as $result) {
    $user_tmp = array();
    $users = tripal_access_restriction_get_users($result->chaco_user_id);
    foreach ($users as $user) {
      $user_tmp[] = $user->name;
    }
    
    $protection_sets = tripal_access_restriction_get_protection_sets();
    $nb_specific_access = 0;
    foreach ($protection_sets as $protection_set) {
      $nb_specific_access += chado_query('SELECT COUNT(1) FROM {chaco_' . $protection_set->main_table . '_access} WHERE chaco_user_id = :user_id AND access_level > 0;',
        array('user_id' => $result->chaco_user_id))->fetchField();
    }
    
    $rows[$result->chaco_user_id] = array (
      'group_name' => $result->name,
      'users' => implode(', ', $user_tmp),
      'specific_access' => $nb_specific_access,
      'operation' => l(t('Edit'), 'admin/tripal/extension/tripal_access_restriction/groups/' . $result->chaco_user_id . '/edit')
        . ' / ' . l(t('Manage'), 'admin/tripal/extension/tripal_access_restriction/groups/' . $result->chaco_user_id . '/manage')
        . '<br>' . l(t('Clear access'), 'admin/tripal/extension/tripal_access_restriction/groups/' . $result->chaco_user_id . '/clear')
        . '<br>' . l(t('Remove'), 'admin/tripal/extension/tripal_access_restriction/groups/' . $result->chaco_user_id . '/remove'),
    );
  }  
  
  // Header of the table
  $header = array(
    'group_name' => t('Group'),
    'users' => t('Users'),
    'specific_access' => t('Specific Access'),
    'operation' => t('Operation'),
  );
  
  // Table which contains all groups
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('No group found'), 
  ));
  
  return $output;
}

/**
 * Implements hook_form().
 *
 * Form to add or update a group.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_update_form($form, &$form_state, $op = 'add', $group_id = NULL) {
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Chado Controller Access Restriction'), 'admin/tripal/extension/tripal_access_restriction');
  $crumbs[] = l(t('Groups'), 'admin/tripal/extension/tripal_access_restriction/groups');
  drupal_set_breadcrumb($crumbs);
  
  // If op == edit we load the current data about the group
  if ($op == 'edit' && isset($group_id)) {
    $sql = 'SELECT * FROM {chaco_user} WHERE chaco_user_id = :group_id';
    $result = chado_query($sql, array('group_id' => $group_id))->fetch();
    if (empty($result)) {
      $op = 'add';
    }
  } else {
    $op = 'add';
  }
  
  $form = array(
    '#attributes' => array('class' => 'tripal_access_restriction_protection_sets_exposed_form'),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Group name'),
  );
  
  $form['comment'] = array(
    '#type' => 'textfield',
    '#title' => t('Administrative Comment'),
  );
  
  if ($op == 'edit') {
    $form['name']['#default_value'] = $result->name;
    $form['comment']['#default_value'] = $result->comment;
    $form['group_id'] = array(
      '#type' => 'hidden',
      '#default_value' => $result->chaco_user_id,
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t(ucfirst($op) . ' group'),
    '#id' => $op . '-submit',
  );
  
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_groups_update_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_update_form_submit($form, &$form_state) {
  
  $params = array(
    'name' => $form_state['values']['name'],
    'flags' => tripal_access_restriction_get_flag_group(),
    'comment' => $form_state['values']['comment'],
  );
  
  if ($form_state['clicked_button']['#id'] === 'add-submit') {
    $sql = 'INSERT INTO {chaco_user}("name", "flags", "comment") VALUES (:name, :flags, :comment);';
    chado_query($sql, $params);
    drupal_set_message('Group ' . $params['name'] . ' created.');
    $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/groups';
  }
  
  if ($form_state['clicked_button']['#id'] === 'edit-submit') {
    $params['group_id'] = $form_state['values']['group_id'];
    $sql = 'UPDATE {chaco_user} SET name = :name, flags = :flags, comment = :comment WHERE chaco_user_id = :group_id';
    chado_query($sql, $params);
    drupal_set_message('Group ' . $params['name'] . ' updated.');
  }
  
}

/**
 * Implements hook_form().
 *
 * Form to manage a group.
 *
 * @return
 *   The form
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_manage_form($form, &$form_state, $group_id = NULL) {
  $crumbs = array();
  $crumbs[] = l(t('Home'), '<front>');
  $crumbs[] = l(t('Chado Controller Access Restriction'), 'admin/tripal/extension/tripal_access_restriction');
  $crumbs[] = l(t('Groups'), 'admin/tripal/extension/tripal_access_restriction/groups');
  drupal_set_breadcrumb($crumbs);
  
  $form = array(
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_exposed_form')),
  );
  
  // Get data about the group
  $data  = chado_query('SELECT * FROM {chaco_user} WHERE chaco_user_id = :group_id', array('group_id' => $group_id))->fetch();
  if (empty($data)) {
    drupal_set_message('', 'error');
    $form_state['redirect'] = '';
    return $form;
  }
  
  // Group name field
  $form['group_name'] = array(
    '#type' => 'container',
    '#prefix' => '<b>' . t('Group name: ') . '</b>' . $data->name . ' (' 
      . l(t('settings'), 'admin/tripal/extension/tripal_access_restriction/groups/' . $data->chaco_user_id . '/edit') . ')',
    '#attributes' => array('style' => 'display:block; margin-bottom: 1em;')
  );
  
  // Users field
  $options = array();
  $users = tripal_access_restriction_get_users();
  foreach ($users as $user) {
    $options[$user->chaco_user_id] = $user->name;
  }
  $form['users'] = array(
    '#type' => 'select',
    '#multiple' => true,
    '#size' => 10,
    '#title' => t('Users'),
    '#description' => t('Hold down the Ctrl (Windows) / Command (Mac) button to select multiple options.'),
    '#options' => $options,
    '#attributes' => array('class' => array('tripal_access_restriction_protection_sets_form')),
  );
  $default = array();
  $users = tripal_access_restriction_get_users($data->chaco_user_id);
  foreach ($users as $user) {
    $default[] = $user->chaco_user_id;
  }
  $form['users']['#default_value'] = $default;
  
  // Protection sets fields
  $protection_sets = tripal_access_restriction_get_protection_sets();
  foreach ($protection_sets as $protection_set) {
    
    if (empty($protection_set->management_view)) {
      continue;
    }
    
    // Create the collapsed block
    $form['collapsed-' . $protection_set->management_view] = array(
      '#type' => 'fieldset', 
      '#title' => t('ChaCo ' . ucfirst(str_replace('_' , ' ', $protection_set->main_table))), 
      '#collapsible' => TRUE, 
      '#collapsed' => TRUE,
    );
    
    // Load the view
    $view = views_get_view($protection_set->management_view, 'default');
    
    if (empty($view)) {
      unset($form['collapsed-' . $protection_set->main_table]);
      continue;
    }
    
    // Add php fields to the view
    $view->display['default']->display_options['fields'][$protection_set->key . '_none'] = array(
       'id'    => 'php',
       'table' => 'views',
       'field' => 'php',
       'label' => t('None'),
       'use_php_setup' => 0,
       'php_output' => '<input type="radio" name="tripal_access_config[' . $protection_set->main_table . ']<?php echo \'[\' . $row->' . $protection_set->key . ' . \']\'; ?>" value="0" <?php if ($data->chaco_access_access_level == 0)  echo \'checked\'; ?> >',
       'use_php_click_sortable' => '0',
       'php_click_sortable' => '',
    );
    $view->display['default']->display_options['fields'][$protection_set->key . '_read'] = array(
       'id'    => 'php',
       'table' => 'views',
       'field' => 'php',
       'label' => t('Read'),
       'use_php_setup' => 0,
       'php_output' => '<input type="radio" name="tripal_access_config[' . $protection_set->main_table . ']<?php echo \'[\' . $row->' . $protection_set->key . ' . \']\'; ?>" value="1" <?php if ($data->chaco_access_access_level == 1)  echo \'checked\'; ?>>',
       'use_php_click_sortable' => '0',
       'php_click_sortable' => '',
    );
    $view->display['default']->display_options['fields'][$protection_set->key . '_write'] = array(
       'id'    => 'php',
       'table' => 'views',
       'field' => 'php',
       'label' => t('Write'),
       'use_php_setup' => 0,
       'php_output' => '<input type="radio" name="tripal_access_config[' . $protection_set->main_table . ']<?php echo \'[\' . $row->' . $protection_set->key . ' . \']\'; ?>" value="3" <?php if ($data->chaco_access_access_level > 1)  echo \'checked\'; ?>>',
       'use_php_click_sortable' => '0',
       'php_click_sortable' => '',
    );
    
    // Build the view query
    $view->build();
    
    // Pick the table which has the key field for the join
    $main_table = $protection_set->main_table;
    foreach ($view->query->fields as $field) {
        if ($field['field'] == $protection_set->key) {
          $main_table = $field['table'];
          break;
        }
    }
        
    // Add the join with the access table
    $join = new views_join();
    $extra = array(
      array(
        'field' => 'chaco_user_id',
        'table' => 'chaco_access',
        'value' => $data->chaco_user_id,
      ),
    );
    $join->construct('chaco_' . $protection_set->main_table . '_access', $main_table, $protection_set->key, $protection_set->key, $extra);
        
    $view->query->add_relationship('chaco_access', $join, 'chaco_' . $protection_set->main_table . '_access');
    $view->query->add_field('chaco_access', 'access_level');
    
    // Rebuild the query
    $view->query->build($view);
    
    // Alteration of the exposed filter, if exists, to avoid conflict with the main form
    // Remove form tags (<form ...> and </form>) to avoid nested form
    // FIXME : find a better way to achieve that
    if (isset($view->exposed_widgets)) {
      $view->exposed_widgets = preg_replace('~<form(.*?)>~', '', $view->exposed_widgets);
      $view->exposed_widgets = preg_replace('~</form(.*?)>~', '', $view->exposed_widgets);
      // Input 'q' of the exposed filter
      $view->exposed_widgets = preg_replace('~<input type="hidden" name="q" value="\K[^"]+~', 'admin/tripal/extension/tripal_access_restriction/groups/' . $group_id . '/manage', $view->exposed_widgets);      
    }
    
    // Add view in the collapsed block
    $form['collapsed-' . $protection_set->management_view]['admin-view'] = array(
      '#type' => 'markup',
      '#markup' => $view->preview(),
    );  

    $view->destroy();
  }
  
  // Submit button    
  $form['update-submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update access'),
    '#id' => 'update-submit',
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_groups_manage_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_manage_form_submit($form, &$form_state) {  
  $group_id = $form_state['build_info']['args'][0];
  
  if (isset($form_state['input']['op'])) {
    // Set group right for each selected rows in each views
    foreach ($form_state['input']['tripal_access_config'] as $main_table => $data) {
      foreach ($data as $key_id => $access_level) {
        tripal_access_restriction_set_user_right($main_table, $key_id, $group_id, $access_level);
      }
    }
    
    $users = $form_state['values']['users'];
    if (empty($users)) {
      $users = array();
    }
    
    // Remove users from the group
    $current_users = tripal_access_restriction_get_users($group_id);
    $current_users_id = array();
    foreach ($current_users as $current_user) {
      $current_users_id[$current_user->user_id] = $current_user;
    }
    $users_to_remove = array_diff_key($current_users_id , $users);
    foreach ($users_to_remove as $user_to_remove) {
      tripal_access_restriction_remove_user_from_group($user_to_remove->chaco_user_id, $group_id);
    }
    
    // Add users to the group
    foreach ($users as $user) {
      tripal_access_restriction_add_user_to_group($user, $group_id);
    }
  
    drupal_set_message('Group data saved.');
  }
  
  // Build the url
  $query = array();
  $fields_to_ignore = array('users' => true, 'q' => true, 'tripal_access_config' => true, 'form_build_id' => true, 'form_token' => true, 'form_id' => true, 'op' => true);
  $fields_from_views = array_diff_key($form_state['input'], $fields_to_ignore);
  foreach ($fields_from_views as $id => $field) {
    if (!empty($field) || $field === 0) {
      $query[$id] = $field;
    }
  }
  
  drupal_goto('admin/tripal/extension/tripal_access_restriction/groups/' . $group_id . '/manage', array('query' => $query));
}

/**
 * Implements hook_form().
 *
 * Confirmation form when a user wants to remove a group.
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_remove_form($form, &$form_state, $group_id) {
  $form_state['group_id'] = $group_id;
  return confirm_form($form,
    t('Are you sure you want to delete this group ?'),
    'admin/tripal/extension/tripal_access_restriction/groups',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Implements hook_form_submit().
 *
 * Submit callback function of the form "tripal_access_restriction_groups_remove_form".
 *
 * @ingroup tripal_access_restriction
 */
function tripal_access_restriction_groups_remove_form_submit($form, &$form_state) {
  $sql = 'DELETE FROM {chaco_user} WHERE chaco_user_id = :group_id';
  chado_query($sql, array('group_id' => $form_state['group_id']));
  drupal_set_message('Group deleted.');
  $form_state['redirect'] = 'admin/tripal/extension/tripal_access_restriction/groups';
}